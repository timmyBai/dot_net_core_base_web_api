using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.IO.Compression;

using DotNetCoreBaseWebApi.Models;
using DotNetCoreBaseWebApi.Middleware;
using System.Reflection;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.RateLimiting;
using System.Threading.RateLimiting;
using PasetoBearer.Authentication;
using DotNetCoreBaseWebApi.Services;

var DotNetCoreBaseWeb = "dotNetCoreBaseWeb";
var DotNetCoreBaseWebUrl = "http://localhost:3000";
string? environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

if (environment == null)
{
    throw new InvalidOperationException("Not Set 'ASPNETCORE_ENVIRONMENT' Variable");
}

var builder = WebApplication.CreateBuilder(args);

// 加入 控制器
builder.Services.AddControllers();

// 加入 api 資源管理器
builder.Services.AddEndpointsApiExplorer();

// 加入註冊 IHttpContextAccessor 服务
builder.Services.AddHttpContextAccessor();

// 加入跨域
builder.Services.AddCors(policy =>
{
    policy.AddPolicy(name: DotNetCoreBaseWeb, builder =>
    {
        builder.WithOrigins(DotNetCoreBaseWebUrl).WithMethods("GET", "POST", "PUT", "PATCH", "DELETE")
            .WithHeaders("Content-Type")
            .WithHeaders("Authorization")
            .WithHeaders("X-Requested-With")
            .WithHeaders("Accept")
            .WithHeaders("Origin");
    });
});

// 加入限流
builder.Services.AddRateLimiter(rateLimiterConfig =>
{
    // 使用固定時間限制請求數量
    rateLimiterConfig.AddFixedWindowLimiter("fixed", fixedConfig =>
    {
        fixedConfig.PermitLimit = 4;
        fixedConfig.Window = TimeSpan.FromSeconds(60);
        fixedConfig.QueueLimit = 2;
        fixedConfig.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        fixedConfig.AutoReplenishment = true;
    });

    // 滑動窗口限流器
    rateLimiterConfig.AddSlidingWindowLimiter(policyName: "sliding", slidingConfig =>
    {
        slidingConfig.PermitLimit = 100;
        slidingConfig.Window = TimeSpan.FromSeconds(30);
        slidingConfig.QueueLimit = 2;
        slidingConfig.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        slidingConfig.AutoReplenishment = true;
        slidingConfig.SegmentsPerWindow = 3;
    });

    // 令牌限流器
    rateLimiterConfig.AddTokenBucketLimiter(policyName: "tokenBucket", tokenBucketConfig =>
    {
        tokenBucketConfig.TokenLimit = 4;
        tokenBucketConfig.ReplenishmentPeriod = TimeSpan.FromSeconds(10);
        tokenBucketConfig.TokensPerPeriod = 2;
        tokenBucketConfig.QueueLimit = 2;
        tokenBucketConfig.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        tokenBucketConfig.AutoReplenishment = true;
    });

    // 併發限流器
    rateLimiterConfig.AddConcurrencyLimiter(policyName: "concurrency", concurrencyConfig =>
    {
        concurrencyConfig.PermitLimit = 4;
        concurrencyConfig.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
        concurrencyConfig.QueueLimit = 2;
    });
});

// 加入 API 回應壓縮
builder.Services.AddResponseCompression(options =>
{
    options.EnableForHttps = true;
    options.Providers.Add<BrotliCompressionProvider>();
    options.Providers.Add<GzipCompressionProvider>();
});

// 加入 Brotli 壓縮提供者
builder.Services.Configure<BrotliCompressionProviderOptions>(options =>
{
    options.Level = CompressionLevel.Fastest;
});

// 加入 Gzip 壓縮提供者
builder.Services.Configure<GzipCompressionProviderOptions>(options =>
{
    options.Level = CompressionLevel.SmallestSize;
});

// 加入 swagger
builder.Services.AddSwaggerGen(config =>
{
    string projectName = $"Dot Net Core Base Web Api {environment}";
    string packageJson = $@"{AppDomain.CurrentDomain.BaseDirectory}package.json";
    StreamReader sr = new StreamReader(packageJson);
    string fileText = sr.ReadToEnd();
    var packageJsonKey = JsonConvert.DeserializeObject<PackageJsonModels>(fileText);
    string major = packageJsonKey.version.Split(".")[0];

    string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    config.IncludeXmlComments(xmlPath);

    config.SwaggerDoc($@"v{major}", new OpenApiInfo()
    {
        Title = projectName,
        Version = packageJsonKey.version,
        Description = packageJsonKey.description,
        Contact = new OpenApiContact()
        {
            Email = "supper@gmail.com",
        }
    });

    config.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "PASETO",
        In = ParameterLocation.Header,
        Description = "PASETO Authorization"
    });

    config.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] {}
        }
    });
});

// 加入 paseto 驗證
builder.Services
    .AddAuthorization(options =>
    {
        options.AddPolicy("Permission", policy => policy.Requirements.Add(new AuthRequirement()));
    })
    .AddAuthentication(s =>
    {
        s.DefaultAuthenticateScheme = PasetoBearerDefaults.AuthenticationScheme;
        s.DefaultScheme = PasetoBearerDefaults.AuthenticationScheme;
        s.DefaultChallengeScheme = PasetoBearerDefaults.AuthenticationScheme;
    });

// 加入 Paseto 服務
builder.Services.AddSingleton<PasetoService>();

// 加入授權驗證 服務
builder.Services.AddSingleton<IAuthorizationHandler, AuthHandler>();

var app = builder.Build();

// 如果 api 為 development 或 staging 模式，啟用 swagger
if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
{
    app.UseSwagger(options =>
    {
        options.RouteTemplate = "DotNetCoreBaseWebApi/swagger/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(options =>
    {
        options.RoutePrefix = "DotNetCoreBaseWebApi/swagger";
        options.SwaggerEndpoint("/DotNetCoreBaseWebApi/swagger/v1/swagger.json", "v1");
    });
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

// 使用跨域
app.UseCors(DotNetCoreBaseWeb);

// 使用 websocket
app.UseWebSockets(new WebSocketOptions()
{
    KeepAliveInterval = TimeSpan.FromSeconds(120)
});

// 使用 API 回應壓縮
app.UseResponseCompression();

// 使用 https 重新導向
//app.UseHttpsRedirection();

// 使用限流
app.UseRateLimiter();

app.UseStaticFiles();

// 使用認證
app.UseAuthentication();

// 使用授權
app.UseAuthorization();

app.MapControllers();

app.Run();
