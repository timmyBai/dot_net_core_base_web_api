using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DotNetCoreBaseWebApi.Utils
{
    public class JwtUtils
    {
        /// <summary>
        /// 私鑰
        /// </summary>
        public const string SecurityKey = "}ORB.c0>R_6gIAx=6m93mk9DZX3~sp7\\x#BVh'=n7u*pl.pR<+}<Z>ZEWF/QBWu$-jB.G#M1`jk1<x7|b>;u}A!3F]v;@.E:[}]$Kn=?i>Ee/vWXH`9q+1HX\\VVC$=X#>Hh[\"9*bw|x>W8B({euvvq%`i(R3n*_hI7-d}PI|p4n=^h1FdDF+/wK/`.J%u$>jzC@{1[@=hXK9c.^4L)jQQ]%ssu\".Vnh7*VMLC%:M|.-=~zWAdhh@,/P^@>H+J)_+";

        /// <summary>
        /// 網域
        /// </summary>
        public const string Domain = "http://localhost:5000/";

        /// <summary>
        /// 亂數預備字串
        /// </summary>
        private const string randomCode = "ABCDEFGHIJKLMNOPQRSTUVWXZYabcdefghijklmnopqustuvwxyz0123456789~`!@#$%^&*()_-+={[}]|<,>.?/:;'";

        /// <summary>
        /// 時效分鐘
        /// </summary>
        public const int Minutes = 30;

        /// <summary>
        /// 生成 refresh token
        /// </summary>
        /// <returns></returns>
        public string GenerateRefreshToken()
        {
            string refreshToken = "";

            for (int i = 0; i < 256; i++)
            {
                Random random = new Random();
                int index = random.Next(0, randomCode.Length);
                refreshToken += randomCode[index];
            }

            return refreshToken;
        }

        /// <summary>
        /// 生成 jwt 金鑰
        /// </summary>
        /// <param name="claim">加密項目</param>
        /// <param name="audience">接收者</param>
        /// <returns></returns>
        public string GenerateJwtToken(IEnumerable<Claim> claim)
        {
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: Domain,
                    expires: DateTime.Now.AddMinutes(Minutes),
                    signingCredentials: creds,
                    claims: claim
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// jwt 解密
        /// </summary>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        public Dictionary<string, string> DecryptJwtKey(string token)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            TokenValidationParameters param = new TokenValidationParameters();
            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));

            param.IssuerSigningKey = symmetricSecurityKey;
            param.ValidateIssuer = false;
            param.ValidateAudience = false;
            tokenHandler.InboundClaimTypeMap.Clear();
            ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, param, out SecurityToken securityToken);

            foreach (var item in claimsPrincipal.Claims)
            {
                result.Add(item.Type, item.Value);
            }

            return result;
        }
    }
}
