using System.Diagnostics;
using System.Net.WebSockets;
using System.Text;

namespace DotNetCoreBaseWebApi.Utils
{
    public class WebSocketUtils
    {
        /// <summary>
        /// 處理 websocket 訊息
        /// </summary>
        /// <param name="webSocket"></param>
        /// <returns></returns>
        public async Task HandleWebSocketMessage(WebSocket webSocket)
        {
            var buffer = new byte[1024 * 4];
            var receiveResult = await webSocket.ReceiveAsync(
                new ArraySegment<byte>(buffer), CancellationToken.None);

            while (!receiveResult.CloseStatus.HasValue)
            {
                if (receiveResult.MessageType == WebSocketMessageType.Text)
                {
                    string message = System.Text.Encoding.UTF8.GetString(buffer, 0, receiveResult.Count);
                    byte[] messageBuffer = new byte[] { };

                    if (message == "送出蘋果")
                    {
                        messageBuffer = Encoding.UTF8.GetBytes("謝謝");

                        await webSocket.SendAsync(
                            new ArraySegment<byte>(messageBuffer, 0, messageBuffer.Length),
                            receiveResult.MessageType,
                            receiveResult.EndOfMessage,
                            CancellationToken.None);
                    }
                    else if (message == "送出草莓")
                    {
                        messageBuffer = Encoding.UTF8.GetBytes("超級感謝");

                        await webSocket.SendAsync(
                            new ArraySegment<byte>(messageBuffer, 0, messageBuffer.Length),
                            receiveResult.MessageType,
                            receiveResult.EndOfMessage,
                            CancellationToken.None);
                    }
                    else if (message == "送出蟑螂")
                    {
                        messageBuffer = Encoding.UTF8.GetBytes("打死你");

                        await webSocket.SendAsync(
                            new ArraySegment<byte>(messageBuffer, 0, messageBuffer.Length),
                            receiveResult.MessageType,
                            receiveResult.EndOfMessage,
                            CancellationToken.None);
                    }
                    else
                    {
                        await webSocket.SendAsync(
                        new ArraySegment<byte>(buffer, 0, receiveResult.Count),
                        receiveResult.MessageType,
                        receiveResult.EndOfMessage,
                        CancellationToken.None);
                    }
                }

                receiveResult = await webSocket.ReceiveAsync(
                    new ArraySegment<byte>(buffer), CancellationToken.None);
            }

            await webSocket.CloseAsync(
                receiveResult.CloseStatus.Value,
                receiveResult.CloseStatusDescription,
                CancellationToken.None);
        }
    }
}
