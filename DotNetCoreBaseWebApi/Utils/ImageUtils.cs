using iTextSharp.text;

namespace DotNetCoreBaseWebApi.Utils
{
    public class ImageUtils
    {
        /// <summary>
        /// 支援圖片副檔
        /// </summary>
        private string[] FileExtension = new string[] {
            "data:image/png;base64,",
            "data:image/gif;base64,",
            "data:image/jpg;base64,",
            "data:image/jpeg;base64,"
        };

        /// <summary>
        /// 檢查是否為合法副檔名
        /// </summary>
        /// <param name="base64Image">base64 圖片</param>
        /// <returns></returns>
        public bool InspactPictureFileExtension(string base64Image)
        {
            for (int i = 0; i < FileExtension.Length; i++)
            {
                if (base64Image.IndexOf(FileExtension[i]) != -1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 檢查圖片容量大小
        /// </summary>
        /// <param name="base64Image">base64Image</param>
        /// <returns></returns>
        public double GetPictureSize(string base64Image)
        {
            string tempBase64Image = base64Image;
            FileExtension.ToList().ForEach((item) =>
            {
                tempBase64Image = tempBase64Image.Replace(item, "");
            });

            decimal stringLength = tempBase64Image.Length;
            decimal base64FileByte = stringLength - Math.Ceiling(stringLength / 8) * 2;
            decimal base64FileKB = base64FileByte / 1024;
            //decimal base64FileMB = base64FileKB / 1024;

            return Math.Round((double)base64FileKB, 2);
        }
    }
}
