namespace DotNetCoreBaseWebApi.VM.Auth
{
    public class AuthTokenVM
    {
        /// <summary>
        /// 核發 token
        /// </summary>
        public string accessToken { get; set; } = "";

        /// <summary>
        /// 核發 refresh token
        /// </summary>
        public string refreshToken { get; set; } = "";

        /// <summary>
        /// 過期時間
        /// </summary>
        public long expiresIn { get; set; } = 0;

        /// <summary>
        /// token 類型
        /// </summary>
        public string tokenType { get; set; } = "";
    }

    /// <summary>
    /// redis 注入使用者登入模型
    /// </summary>
    public class RedisAuthLoginVM
    {
        /// <summary>
        /// 令牌
        /// </summary>
        public string accessToken { get; set; } = "";
        
        /// <summary>
        /// 重製令牌
        /// </summary>
        public string refreshToken { get; set; } = "";

        /// <summary>
        /// 角色
        /// </summary>
        public string _role { get; set; } = "";

        /// <summary>
        /// 令牌過期時間
        /// </summary>
        public long expiresIn { get; set; } = 0;

        /// <summary>
        /// 令牌類型
        /// </summary>
        public string tokenType { get; set; } = "";

        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; } = "";
    }
}
