using DotNetCoreBaseWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using Microsoft.AspNetCore.Cors;

using Microsoft.AspNetCore.Authorization;
using QRCoder;
using SkiaSharp;
using Microsoft.AspNetCore.RateLimiting;


namespace DotNetCoreBaseWebApi.Controllers.QRCode
{
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class QRCodeController : ControllerBase
    {
        private readonly ILogger<QRCodeController> logger;

        public QRCodeController(ILogger<QRCodeController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 生成 qrcode
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/qrcode/generator")]
        public IActionResult GenerateQRCode()
        {
            ResultModels result = new ResultModels();

            try
            {
                string data = JsonConvert.SerializeObject(new
                {
                    account = "test",
                    password = "test",
                    email = "test@gmail.com",
                    username = "tim",
                    gender = "男"
                });

                // 產生 qrcode 載體
                QRCodeGenerator QRCodeGenerator = new QRCodeGenerator();

                // 產生 qrcode 資料
                QRCodeData QRCodeData = QRCodeGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.M);

                // 轉換 base64 png 圖片
                PngByteQRCode pngByteQrCode = new PngByteQRCode(QRCodeData);
                byte[] QRCodeImageBytes = pngByteQrCode.GetGraphic(10);

                // 建立 qrcode 空白點陣圖，並將 base64 png qrcode 塞入點陣圖
                SKBitmap QRCodeSKBitmap;
                using (MemoryStream stream = new MemoryStream(QRCodeImageBytes))
                {
                    QRCodeSKBitmap = SKBitmap.Decode(stream);
                }

                // 建立純空白點陣圖
                using(SKBitmap wholeQrcode = new SKBitmap(QRCodeSKBitmap.Width, QRCodeSKBitmap.Height))
                {
                    // 建立空白 canvas
                    using(SKCanvas canvas = new SKCanvas(wholeQrcode))
                    {
                        // 加入 qrcode 圖片
                        SKPaint QRCocdePaint = new SKPaint()
                        {
                            Color = SKColors.Black,
                            IsAntialias = true
                        };

                        SKPoint QRCodePoint = new SKPoint();
                        QRCodePoint.X = 0;
                        QRCodePoint.Y = 0;

                        canvas.DrawBitmap(QRCodeSKBitmap, QRCodePoint, QRCocdePaint);

                        // 加入 logo 圖片
                        SKBitmap QRCodeLogo = SKBitmap.Decode($"{AppDomain.CurrentDomain.BaseDirectory}Assets/logo.png");
                        SKBitmap scaleQRCodeLogo = new SKBitmap(wholeQrcode.Width / 10 * 2, wholeQrcode.Height / 10 * 2);
                        QRCodeLogo.ScalePixels(scaleQRCodeLogo, SKSamplingOptions.Default);
                        
                        float SKRectLeft = (wholeQrcode.Width - scaleQRCodeLogo.Width) / 2 - 10;
                        float SKRectTop = (wholeQrcode.Height - scaleQRCodeLogo.Height) / 2 - 10;
                        float SKRectRight = SKRectLeft + scaleQRCodeLogo.Width + 10;
                        float SKRectBottom = SKRectTop + scaleQRCodeLogo.Height + 10;

                        // 加入背景方塊
                        SKRect SKRectBox = new SKRect(SKRectLeft, SKRectTop, SKRectRight, SKRectBottom);
                        SKPaint SKPaintBox = new SKPaint();
                        SKPaintBox.Color = SKColors.White;

                        canvas.DrawRoundRect(SKRectBox, 10, 10, SKPaintBox);

                        canvas.DrawBitmap(scaleQRCodeLogo, SKRectLeft + 5, SKRectTop + 5);

                        // 加入 qrcode 文字資訊
                        using (var SKPaintText = new SKPaint())
                        {
                            SKPaintText.Color = SKColors.Black; // 文字颜色
                            SKPaintText.IsAntialias = true;
                            SKPaintText.Style = SKPaintStyle.Fill;

                            SKFont SKFontText = new SKFont();
                            SKFontText.Size = 20;
                            
                            SKPoint SKPointText = new SKPoint();
                            SKPointText.X = 0;
                            SKPointText.Y = wholeQrcode.Height - 15;

                            canvas.DrawText(data, SKPointText, SKTextAlign.Left, SKFontText, SKPaintText);
                        }

                        using (SKData SKData = wholeQrcode.Encode(SKEncodedImageFormat.Png, 100))
                        {
                            string base64String = Convert.ToBase64String(SKData.ToArray());

                            // 儲存圖片
                            //FileStream fs = new FileStream("test.png", FileMode.Create, FileAccess.Write);
                            //SKData.SaveTo(fs);
                            //SKData.Dispose();
                            //fs.Close();
                            //fs.Dispose();

                            result.isSuccess = true;
                            result.data = "data:image/png;base64," + base64String;
                            return StatusCode((int)HttpStatusCode.OK, result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}
