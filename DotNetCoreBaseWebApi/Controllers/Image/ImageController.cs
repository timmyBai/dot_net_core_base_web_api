using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;

using DotNetCoreBaseWebApi.Models;
using DotNetCoreBaseWebApi.Models.Image;
using DotNetCoreBaseWebApi.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.RateLimiting;

namespace DotNetCoreBaseWebApi.Controllers.Image
{
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly ILogger<ImageController> logger;

        public ImageController(ILogger<ImageController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得圖片容量大小
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/get/image/calc/memory/size")]
        public IActionResult CalcImageMemorySize(RequestCalcImage body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var isIllegalPictures = new ImageUtils().InspactPictureFileExtension(body.picture);

                if (!isIllegalPictures)
                {
                    result.isSuccess = false;
                    result.message = "IllegalPictures";
                    logger.LogError("IllegalPictures");
                    return StatusCode((int)HttpStatusCode.Forbidden, result);
                }

                var memorySize = new ImageUtils().GetPictureSize(body.picture);
                result.isSuccess = true;
                result.data = memorySize;
                
                return StatusCode((int)HttpStatusCode.OK, result);
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                logger.LogError($@"InternalServerError: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}
