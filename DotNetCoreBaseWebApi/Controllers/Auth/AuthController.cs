using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Paseto;
using Paseto.Builder;

using DotNetCoreBaseWebApi.Utils;
using DotNetCoreBaseWebApi.Models;
using DotNetCoreBaseWebApi.Models.Auth;
using DotNetCoreBaseWebApi.VM.Auth;
using DotNetCoreRedisDataRedisClient;
using Microsoft.AspNetCore.RateLimiting;
using DotNetCoreBaseWebApi.Services;
using Microsoft.Extensions.Logging;


namespace DotNetCoreBaseWebApi.Controllers.Auth
{
    /// <summary>
    /// 會員登入與登出
    /// </summary>
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> logger;
        private PasetoService? pasetoService = null;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="_pasetoService">Paseto Token 服務</param>
        /// <param name="_logger"></param>
        public AuthController(PasetoService _pasetoService, ILogger<AuthController> _logger)
        {
            pasetoService = _pasetoService;
            logger = _logger;
        }

        /// <summary>
        /// 會員登入
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("dotNetCoreBaseWeb")]
        [AllowAnonymous]
        [EnableRateLimiting("fixed")]
        [HttpPost]
        [Route("api/auth/login")]
        public IActionResult AuthLogin(RequestAuthLoginModels body)
        {
            ResultModels result = new ResultModels();
            string? environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var userInfo = new AuthModels().InspectUserInfoExist(body.account, body.password);

                if (userInfo.Count() != 1)
                {
                    result.isSuccess = false;
                    result.message = "AccountExist";
                    logger.LogError("AccountExist");
                    return StatusCode((int)HttpStatusCode.Forbidden, result);
                }

                Claim[] claim = new Claim[]
                {
                    new Claim("account", userInfo.ToArray()[0].account),
                    new Claim("userName", userInfo.ToArray()[0].userName),
                    new Claim("_role", userInfo.ToArray()[0].role),
                    new Claim("Aud", userInfo.ToArray()[0].account), // 接收 paseto 一方
                    new Claim("Sub", userInfo.ToArray()[0].userName), // paseto 面相用戶
                };

                var token = pasetoService.Encode(claim, userInfo.ToArray()[0].account, userInfo.ToArray()[0].userName);

                AuthTokenVM authToken = new AuthTokenVM();
                authToken.accessToken = token.Replace(pasetoService.PasetoType, "");
                authToken.refreshToken = pasetoService.GenerateRefreshToken();
                authToken.expiresIn = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));
                authToken.tokenType = "Bearer";

                using (DotNetCoreRedisConnection redis = new DotNetCoreRedisConnection())
                {
                    redis.Connect();
                    redis.GetDatabase(0);

                    var allKeys = redis.GetAllKeys();
                    foreach (var key in allKeys)
                    {
                        var targetKey = redis.GetStringJson<RedisAuthLoginVM>(key);

                        if (targetKey != null && targetKey.account == userInfo[0].account)
                        {
                            redis.DeleteKey(key);
                        }
                    }

                    object authLogin = new
                    {
                        accessToken = pasetoService.PasetoType + authToken.accessToken,
                        refreshToken = authToken.refreshToken,
                        expiresIn = authToken.expiresIn,
                        account = userInfo.ToArray()[0].account,
                        name = userInfo.ToArray()[0].userName,
                        _role = userInfo.ToArray()[0].role,
                        tokenType = "Bearer"
                    };

                    redis.SetStringJson(userInfo[0].account, authLogin, TimeSpan.FromMinutes(PasetoService.Minutes));
                }

                result.isSuccess = true;
                result.data = authToken;
                logger.LogInformation("AuthLoginSuccess");
                return StatusCode((int)HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }

        /// <summary>
        /// 會員簡介
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/auth/introduction")]
        public IActionResult UserIntroduction()
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                string token = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

                var userInfo = pasetoService.Decode(pasetoService.PasetoType + token);

                if (userInfo == null)
                {
                    result.isSuccess = false;
                    result.message = "TokenHasExpired";
                    logger.LogError("TokenHasExpired");
                    return StatusCode((int)HttpStatusCode.Forbidden, result);
                }

                result.isSuccess = true;
                result.data = userInfo;
                logger.LogInformation("GetUserIntroductionSuccess");
                return StatusCode((int)HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                logger.LogError($@"InternalServerError: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }

        /// <summary>
        /// 會員令牌重製
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/auth/refresh")]
        public IActionResult RefrashToken(RequestAuthLoginRefreshModels body)
        {
            ResultModels result = new ResultModels();

            if(!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                using(DotNetCoreRedisConnection redis = new DotNetCoreRedisConnection())
                {
                    redis.Connect();
                    redis.GetDatabase(0);

                    string BearerToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

                    var userInfo = pasetoService.Decode(pasetoService.PasetoType + BearerToken);

                    var payloadKey = redis.GetStringJson<RedisAuthLoginVM>(userInfo["account"].ToString());

                    if (payloadKey == null)
                    {
                        result.isSuccess = false;
                        result.message = "Unauthorized";
                        logger.LogError("Unauthorized");
                        return StatusCode((int)HttpStatusCode.Unauthorized, result);
                    }

                    if (payloadKey.refreshToken != body.refreshToken)
                    {
                        result.isSuccess = false;
                        result.message = "Unauthorized";
                        logger.LogError("Unauthorized");
                        return StatusCode((int)HttpStatusCode.Unauthorized, result);
                    }

                    Claim[] claims = new Claim[]
                    {
                        new Claim("account", userInfo["account"].ToString()),
                        new Claim("userName", userInfo["userName"].ToString()),
                        new Claim("_role", userInfo["_role"].ToString()),
                        new Claim("Aud", userInfo["account"].ToString()), // 接收 paseto 一方
                        new Claim("Sub", userInfo["userName"].ToString()), // paseto 面相用戶
                    };

                    string token = pasetoService.Encode(claims, userInfo["account"].ToString(), userInfo["userName"].ToString());

                    AuthTokenVM authToken = new AuthTokenVM();
                    authToken.accessToken = token.Replace(pasetoService.PasetoType, "");
                    authToken.refreshToken = pasetoService.GenerateRefreshToken();
                    authToken.expiresIn = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));
                    authToken.tokenType = "Bearer";

                    object authLogin = new
                    {
                        accessToken = pasetoService.PasetoType + authToken.accessToken,
                        refreshToken = authToken.refreshToken,
                        expiresIn = authToken.expiresIn,
                        account = userInfo["account"].ToString(),
                        name = userInfo["userName"].ToString(),
                        _role = userInfo["_role"].ToString(),
                        tokenType = "Bearer"
                    };

                    // 存放 redis 新 token 與 refreshToken
                    redis.SetStringJson(userInfo["account"].ToString(), authLogin, TimeSpan.FromMinutes(PasetoService.Minutes));

                    // 刪除舊的 token 與 refreshToken
                    var allKeys = redis.GetAllKeys();
                    foreach (var key in allKeys)
                    {
                        var targetKey = redis.GetStringJson<RedisAuthLoginVM>(key);

                        if (targetKey.refreshToken == body.refreshToken)
                        {
                            redis.DeleteKey(key);
                        }
                    }

                    result.isSuccess = true;
                    result.data = authToken;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                logger.LogError($@"InternalServerError: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }

        /// <summary>
        /// 會員登出
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/auth/logout")]
        public IActionResult Logout()
        {
            ResultModels result = new ResultModels();

            try
            {
                string BearerToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");

                using (DotNetCoreRedisConnection redis = new DotNetCoreRedisConnection())
                {
                    redis.Connect();
                    redis.GetDatabase(0);

                    var userInfo = pasetoService.Decode(pasetoService.PasetoType + BearerToken);

                    var tokenData = redis.GetStringJson<RedisAuthLoginVM>(userInfo["account"].ToString());

                    if (tokenData == null)
                    {
                        result.isSuccess = false;
                        result.message = "TokenExpired";
                        logger.LogError("TokenExpired");
                        return StatusCode((int)HttpStatusCode.Unauthorized, result);
                    }

                    redis.DeleteKey(userInfo["account"].ToString());

                    result.isSuccess = true;
                    result.message = "LogoutSuccess";
                    logger.LogInformation("LogoutSuccess");
                    return StatusCode((int)HttpStatusCode.NoContent);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}
