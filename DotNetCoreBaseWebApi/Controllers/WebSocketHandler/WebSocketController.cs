using Microsoft.AspNetCore.Mvc;

using DotNetCoreBaseWebApi.Utils;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace DotNetCoreBaseWebApi.Controllers.WebSocketHandler
{
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class WebSocketController : ControllerBase
    {
        private readonly ILogger<WebSocketController> logger;

        public WebSocketController(ILogger<WebSocketController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// websocket 處理
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors]
        [HttpGet]
        [Route("ws")]
        public async Task Get()
        {
            if (HttpContext.WebSockets.IsWebSocketRequest)
            {
                using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();

                await new WebSocketUtils().HandleWebSocketMessage(webSocket);
            }
            else
            {
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }
    }
}
