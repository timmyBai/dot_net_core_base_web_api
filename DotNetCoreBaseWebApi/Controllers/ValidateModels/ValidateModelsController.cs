using Microsoft.AspNetCore.Mvc;
using System.Net;

using DotNetCoreBaseWebApi.Models;
using DotNetCoreBaseWebApi.Models.ValidateModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace DotNetCoreBaseWebApi.Controllers.ValidateModels
{
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class ValidateModelsController : ControllerBase
    {
        private readonly ILogger<ValidateModelsController> logger;

        public ValidateModelsController(ILogger<ValidateModelsController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 資料收模型驗證
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [HttpPost]
        [Route("api/validate/model/state")]
        public IActionResult ValidateModelState(RequestValidateStateModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value?.Errors.Count > 0)
                .ToDictionary(k => k.Key, k => k.Value?.Errors.Select(e => e.ErrorMessage)).ToArray();

                result.isSuccess = false;
                result.data = errors;
                result.message = "資料傳輸錯誤";
                logger.LogError("DataRequestError");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            result.isSuccess = true;
            result.message = "資料傳輸成功";
            logger.LogInformation("DataRequestSuccess");
            return StatusCode((int)HttpStatusCode.OK, result);
        }
    }
}
