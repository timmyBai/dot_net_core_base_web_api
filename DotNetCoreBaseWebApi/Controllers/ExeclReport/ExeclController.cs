using DotNetCoreBaseWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using DotNetCoreBaseWebApi.Models.ExcelReport;
using System.Net;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.RateLimiting;

namespace DotNetCoreBaseWebApi.Controllers.ExeclReport
{
    [Route("DotNetCoreBaseWeb")]
    [ApiController]
    public class ExeclController : ControllerBase
    {
        private readonly ILogger<ExeclController> logger;
        
        public ExeclController(ILogger<ExeclController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// excel 報表匯出
        /// </summary>
        /// <returns></returns>
        [Authorize("Permission")]
        [EnableCors("dotNetCoreBaseWeb")]
        [EnableRateLimiting("tokenBucket")]
        [HttpPost]
        [Route("api/execl/report")]
        public IActionResult ExcelReport()
        {
            ResultModels result = new ResultModels();

            try
            {
                var studentData = new ExcelReportModels().GetStudentList();

                HSSFWorkbook hssfworkbook = new HSSFWorkbook(); // 建立活頁簿
                ISheet sheet = hssfworkbook.CreateSheet("學生就讀清單"); // 建立工作表

                IFont cellFont = hssfworkbook.CreateFont();
                cellFont.FontName = "微軟正黑體";
                cellFont.FontHeightInPoints = 20;
                cellFont.Boldweight = (short)FontBoldWeight.Bold;
                
                ICellStyle cellStyle = hssfworkbook.CreateCellStyle();
                cellStyle.Alignment = HorizontalAlignment.Center;
                cellStyle.VerticalAlignment = VerticalAlignment.Center;
                cellStyle.SetFont(cellFont);

                sheet.CreateRow(0); //需先用 CreateRow 建立,才可通過 GetRow 取得該欄位
                sheet.AddMergedRegion(new CellRangeAddress(0, 1, 0, 2)); //合併1~2列及A~C欄儲存格
                sheet.GetRow(0).CreateCell(0).SetCellValue("東海大學");
                sheet.GetRow(0).GetCell(0).CellStyle = cellStyle; //套用樣式
                sheet.CreateRow(2).CreateCell(0).SetCellValue("學生編號");
                sheet.GetRow(2).CreateCell(1).SetCellValue("學生姓名");
                sheet.GetRow(2).CreateCell(2).SetCellValue("就讀科系");

                int rowIndex = 3;
                for (int row = 0; row < studentData.Count(); row++)
                {
                    sheet.CreateRow(rowIndex).CreateCell(0).SetCellValue(studentData[row].Id);
                    sheet.GetRow(rowIndex).CreateCell(1).SetCellValue(studentData[row].Name);
                    sheet.GetRow(rowIndex).CreateCell(2).SetCellValue(studentData[row].Department);
                    rowIndex++;
                }

                MemoryStream excelDatas = new MemoryStream();
                hssfworkbook.Write(excelDatas);

                return File(excelDatas.ToArray(), "application/vnd.ms-excel", "學生資料.xls");
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}
