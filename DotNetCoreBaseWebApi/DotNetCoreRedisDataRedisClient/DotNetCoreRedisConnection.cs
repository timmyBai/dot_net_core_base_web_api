using DotNetCoreBaseWebApi.Models;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Text.RegularExpressions;

namespace DotNetCoreRedisDataRedisClient
{
    public class DotNetCoreRedisConnection : IDisposable
    {
        /// <summary>
        /// redis 連線 class
        /// </summary>
        private ConnectionMultiplexer? Connection;

        /// <summary>
        /// redis 資料庫介面
        /// </summary>
        private IDatabase? Database;

        /// <summary>
        /// redis 連線初始化
        /// </summary>
        public DotNetCoreRedisConnection()
        {
            Connection = null;
        }

        /// <summary>
        /// 初始化連線
        /// </summary>
        public void Connect()
        {
            Connection = ConnectionMultiplexer.Connect(GetConnectionString());
        }

        /// <summary>
        /// 取得連線資訊
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            IConfigurationRoot configuration = new ConfigurationModels().Build();
            string Host = configuration.GetValue<string>("Redis:Host");
            string Port = configuration.GetValue<string>("Redis:Port");
            return $@"{Host}:{Port}";
        }

        /// <summary>
        /// 檢查 redis 連線狀態
        /// </summary>
        /// <returns></returns>
        public bool Status()
        {
            bool isActive = Regex.IsMatch(Connection.GetStatus(), "active");

            if (isActive)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 釋放資料庫資源
        /// </summary>
        public void Dispose()
        {
            if (Connection != null)
            {
                Connection.Dispose();
            }
        }

        /// <summary>
        /// 關閉資料庫
        /// </summary>
        public void Close()
        {
            Connection.Close();
        }

        /// <summary>
        /// 取得 redis 資料庫索引
        /// </summary>
        /// <param name="db"></param>
        public void GetDatabase(int db)
        {
            if (db >= 0 && db <= 15)
            {
                Database = Connection?.GetDatabase(db);
            }
        }

        /// <summary>
        /// 設定 redis 鍵值 json 字串格式
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <param name="value">設定值</param>
        public void SetStringJson(RedisKey key, object value, TimeSpan? expiry = null)
        {
            string json = JsonConvert.SerializeObject(value);

            Database.StringSet(key, json, expiry);
        }

        /// <summary>
        /// 取得 redis 鍵值 json 字串格式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">鍵值</param>
        /// <returns></returns>
        public T? GetStringJson<T>(RedisKey key)
        {
            RedisValue json = Database.StringGet(key);

            if (json.IsNull)
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 設定 redis 鍵值字串格式
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <param name="value">設定值</param>
        public void SetString(RedisKey key, RedisValue value, TimeSpan? expiry = null)
        {
            Database.StringSet(key, value, expiry);
        }

        /// <summary>
        /// 取得 redis 鍵值字串格式
        /// </summary>
        /// <param name="key">鍵值</param>
        /// <returns></returns>
        public string? GetString(RedisKey key)
        {
            RedisValue value = Database.StringGet(key);

            if (value.IsNull)
            {
                return null;
            }

            return value;
        }

        /// <summary>
        /// 刪除 redis 鍵值
        /// </summary>
        /// <param name="key">鍵值</param>
        public void DeleteKey(RedisKey key)
        {
            Database.KeyDelete(key);
        }

        /// <summary>
        /// 取得 redis 所有鍵值
        /// </summary>
        /// <returns></returns>
        public RedisKey[] GetAllKeys()
        {
            return (RedisKey[])Database.Execute("KEYS", "*");
        }

        /// <summary>
        /// 設定 hash 鍵值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        /// <param name="value"></param>
        public void SetHash(RedisKey key, RedisValue hashKey, RedisValue value)
        {
            Database.HashSet(key, hashKey, value);
        }

        /// <summary>
        /// 設定 hash 鍵值支援 hash entry
        /// </summary>
        /// <param name="key"></param>
        /// <param name="entry"></param>
        public void SetHash(RedisKey key, HashEntry[] entry)
        {
            Database.HashSet(key, entry);
        }

        /// <summary>
        /// 設定 hash 鍵值支援 json
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        /// <param name="value"></param>
        public void SetHashJson(RedisKey key, RedisValue hashKey, object value)
        {
            string json = JsonConvert.SerializeObject(value);

            Database.HashSet(key, hashKey, json);
        }

        /// <summary>
        /// 取得 hash 鍵值裡面單一個鍵值內容
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        /// <returns></returns>
        public RedisValue? GetHash(RedisKey key, RedisValue hashKey)
        {
            RedisValue value = Database.HashGet(key, hashKey);

            if (value.IsNull)
            {
                return default(RedisValue);
            }

            return value;
        }

        /// <summary>
        /// 取得 hash 鍵值裡面 json 轉換內容
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        /// <returns></returns>
        public T? GetHashJson<T>(RedisKey key, RedisValue hashKey)
        {
            RedisValue value = Database.HashGet(key, hashKey);

            if (value.IsNull)
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// 取得 hash 鍵值裡面全部鍵值及數值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public HashEntry[] GetHashAll(RedisKey key)
        {
            return Database.HashGetAll(key);
        }

        /// <summary>
        /// 刪除 hash 鍵值內部鍵值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        public void DeleteHash(RedisKey key, RedisValue hashKey)
        {
            Database.HashDelete(key, hashKey);
        }

        /// <summary>
        /// 取得 hash 鍵值長度
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public long HashLength(RedisKey key)
        {
            return Database.HashLength(key);
        }
    }
}
