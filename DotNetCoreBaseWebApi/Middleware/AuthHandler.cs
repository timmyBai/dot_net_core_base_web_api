using DotNetCoreBaseWebApi.Models.Auth;
using DotNetCoreBaseWebApi.Services;
using DotNetCoreBaseWebApi.Utils;
using DotNetCoreRedisDataRedisClient;
using Microsoft.AspNetCore.Authorization;
using Paseto;
using System.Net;

namespace DotNetCoreBaseWebApi.Middleware
{
    /// <summary>
    /// 會員授權中介處理
    /// </summary>
    public class AuthHandler: AuthorizationHandler<AuthRequirement>
    {
        private IHttpContextAccessor iHttpContextAccessor;
        private readonly ILogger<AuthHandler> logger;
        private PasetoService pasetoService;

        /// <summary>
        /// 會員授權初始化
        /// </summary>
        /// <param name="_iHttpContextAccessor">Http Context</param>
        /// <param name="_pasetoService">Paseto 服務</param>
        /// <param name="_logger">日誌紀錄器</param>
        public AuthHandler(IHttpContextAccessor _iHttpContextAccessor, PasetoService _pasetoService, ILogger<AuthHandler> _logger)
        {
            iHttpContextAccessor = _iHttpContextAccessor;
            pasetoService = _pasetoService;
            logger = _logger;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AuthRequirement requirement)
        {
            HttpContext httpContext = iHttpContextAccessor.HttpContext;
            
            try
            {
                bool isAuthenticated = httpContext.Request.Headers["Authorization"].FirstOrDefault() != null;

                if (isAuthenticated)
                {
                    List<UserPermission> userPermissions = requirement.userPermissions;

                    string questToken = httpContext.Request.Headers.Authorization.ToString().Replace("Bearer ", "");

                    PasetoPayload dcryptToken = pasetoService.Decode(pasetoService.PasetoType + questToken);

                    if (dcryptToken == null)
                    {
                        httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        httpContext.Response.WriteAsync("").Wait();
                        logger.LogError("TokenDecryptInvalid");
                        return Task.CompletedTask;
                    }

                    // 如果 token 回去查 redis 不存在強制 401
                    using (DotNetCoreRedisConnection redis = new DotNetCoreRedisConnection())
                    {
                        redis.Connect();
                        redis.GetDatabase(0);

                        bool isExistToken = new AuthModels(redis).GetTokenExist(dcryptToken["account"].ToString());

                        if (!isExistToken)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            httpContext.Response.WriteAsync("").Wait();
                            logger.LogError("TokenSessionInvalid");
                            return Task.CompletedTask;
                        }
                    }

                    string questUrl = httpContext.Request.Path.Value.ToUpperInvariant();

                    // 檢查有權限 url 是否存在
                    bool passUrl = userPermissions.GroupBy(g => g.url).Any(a => a.Key.ToUpperInvariant() == questUrl);

                    // 檢查有 url 使用角色權限有哪些
                    bool passRole = userPermissions.GroupBy(g => g.role).Any(a => a.Key.IndexOf(dcryptToken["_role"].ToString()) != -1 || a.Key.Count() == 0);

                    if (passUrl && passRole)
                    {
                        context.Succeed(requirement);
                    }
                    else
                    {
                        httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        httpContext.Response.WriteAsync("").Wait();
                        logger.LogError("NoPermission");
                        return Task.CompletedTask;
                    }
                }
                else
                {
                    // 檢查系統白名單
                    List<WhiteList> whiteList = requirement.whiteList;

                    string questUrl = httpContext.Request.Path.Value.ToUpperInvariant();

                    bool passUrl = whiteList.GroupBy(g => g.url).Any(a => a.Key.ToUpperInvariant() == questUrl);

                    if (passUrl)
                    {
                        context.Succeed(requirement);
                    }
                    else
                    {
                        context.Fail();
                    }
                }

                return Task.CompletedTask;
            }
            catch(Exception ex)
            {
                httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                httpContext.Response.WriteAsync("").Wait();
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.CompletedTask;
            }
        }
    }
}
