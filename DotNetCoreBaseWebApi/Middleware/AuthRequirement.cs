using Microsoft.AspNetCore.Authorization;

namespace DotNetCoreBaseWebApi.Middleware
{
    public class AuthRequirement : IAuthorizationRequirement
    {
        public List<UserPermission> userPermissions { get; set; }

        public List<WhiteList> whiteList { get; set; }

        public AuthRequirement()
        {
            whiteList = new List<WhiteList>()
            {
                new WhiteList()
                {
                    url = "/DotNetCoreBaseWeb/api/auth/login"
                }
            };

            userPermissions = new List<UserPermission>()
            {
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/itext/sharp/pdf/report",
                    role = new List<string>()
                    {
                        "管理者"
                    }
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/fast/report/pdf/report",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/auth/introduction",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/auth/refresh",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/auth/logout",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/execl/report",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/qrcode/generator",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/validate/model/state",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/api/get/image/calc/memory/size",
                    role = new List<string>()
                },
                new UserPermission()
                {
                    url = "/DotNetCoreBaseWeb/ws",
                    role = new List<string>()
                }
            };
        }
    }

    public class UserPermission
    {
        /// <summary>
        /// 角色
        /// </summary>
        public List<string> role { get; set; }
        
        /// <summary>
        /// Api 網址
        /// </summary>
        public string url { get; set; } = "";
    }

    public class WhiteList
    {
        /// <summary>
        /// Api 網址
        /// </summary>
        public string url { get; set; } = "";
    }
}
