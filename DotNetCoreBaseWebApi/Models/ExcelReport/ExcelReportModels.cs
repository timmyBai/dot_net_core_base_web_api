namespace DotNetCoreBaseWebApi.Models.ExcelReport
{
    public class ExcelReportModels
    {
        /// <summary>
        /// 取得學生清單
        /// </summary>
        /// <returns></returns>
        public List<StudentModels> GetStudentList()
        {
            List<StudentModels> students = new List<StudentModels>();

            for (int i = 1; i <= 6; i++)
            {
                Random random = new Random();
                int randomDepartment = random.Next(1, 3);
                string department = randomDepartment == 1 ? "資管系" : "中文系";
                students.Add(new StudentModels()
                {
                    Id = i,
                    Name = $@"學生 {i} 號",
                    Department = $@"就讀科系: {department}"
                });
            }

            return students;
        }
    }

    /// <summary>
    /// 學生科別模型
    /// </summary>
    public class StudentModels
    {
        /// <summary>
        /// 學生 id
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// 科系
        /// </summary>
        public string Department { get; set; } = "";
    }
}
