using DotNetCoreBaseWebApi.VM.Auth;
using DotNetCoreRedisDataRedisClient;
using System.ComponentModel.DataAnnotations;

namespace DotNetCoreBaseWebApi.Models.Auth
{
    /// <summary>
    /// 登入登出模型
    /// </summary>
    public class AuthModels
    {
        private DotNetCoreRedisConnection? redis = null;

        /// <summary>
        /// 使用者清單
        /// </summary>
        public List<UserInfoModels> userInfoList { get; set; }

        /// <summary>
        /// 初始化生成使用者資料
        /// </summary>
        public AuthModels()
        {
            userInfoList = new List<UserInfoModels>()
            {
                new UserInfoModels()
                {
                    userName = "timmy",
                    account = "timmy",
                    password = "123456",
                    role = "管理者"
                },
                new UserInfoModels()
                {
                    userName = "tim",
                    account = "tim",
                    password = "123456",
                    role = "管理者"
                },
                new UserInfoModels()
                {
                    userName = "king",
                    account = "king",
                    password = "123456",
                    role = "一般使用者"
                },
                new UserInfoModels()
                {
                    userName = "mimic",
                    account = "mimic",
                    password = "123456",
                    role = "一般使用者"
                }
            };
        }

        /// <summary>
        /// 初始化裝載 redis
        /// </summary>
        /// <param name="_redis"></param>
        public AuthModels(DotNetCoreRedisConnection _redis)
        {
            redis = _redis;
        }

        /// <summary>
        /// 檢查使用者帳密是否存在
        /// </summary>
        /// <param name="account"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public List<UserInfoModels> InspectUserInfoExist(string account, string password)
        {
            List<UserInfoModels> userInfo = new List<UserInfoModels>();

            for(int i = 0; i < userInfoList.Count(); i++)
            {
                if (userInfoList[i].account == account && userInfoList[i].password == password)
                {
                    userInfo.Add(new UserInfoModels()
                    {
                        userName = userInfoList[i].userName,
                        account = userInfoList[i].account,
                        password = userInfoList[i].password,
                        role = userInfoList[i].role
                    });
                    break;
                }
            }

            return userInfo;
        }

        /// <summary>
        /// 取得目標 帳號 是否存在
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns></returns>
        public bool GetTokenExist(string? account)
        {
            var result = redis.GetStringJson<RedisAuthLoginVM>(account);

            return result != null;
        }
    }

    /// <summary>
    /// 會員登入 接收模型
    /// </summary>
    public class RequestAuthLoginModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        public string password { get; set; } = "";
    }

    /// <summary>
    /// 重製令牌 接收模型
    /// </summary>
    public class RequestAuthLoginRefreshModels
    {
        /// <summary>
        /// 重製令牌
        /// </summary>
        public string refreshToken { get; set; } = "";
    }

    /// <summary>
    /// 使用者基本資訊
    /// </summary>
    public class UserInfoModels
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string userName { get; set; } = "";
        
        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; } = "";
        
        /// <summary>
        /// 密碼
        /// </summary>
        public string password { get; set; } = "";
        
        /// <summary>
        /// 角色
        /// </summary>
        public string role { get; set; } = "";
    }
}
