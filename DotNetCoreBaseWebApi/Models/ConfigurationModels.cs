namespace DotNetCoreBaseWebApi.Models
{
    public class ConfigurationModels
    {
        public IConfigurationRoot Build()
        {
            var appBuilder = WebApplication.CreateBuilder();

            var app = appBuilder.Build();

            var configurationBuilder = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory);

            if (app.Environment.IsDevelopment())
            {
                configurationBuilder.AddJsonFile("appsettings.Development.json");
            }
            else if (app.Environment.IsStaging())
            {
                configurationBuilder.AddJsonFile("appsettings.Staging.json");
            }
            else
            {
                configurationBuilder.AddJsonFile("appsettings.Production.json");
            }

            return configurationBuilder.Build();
        }
    }
}
