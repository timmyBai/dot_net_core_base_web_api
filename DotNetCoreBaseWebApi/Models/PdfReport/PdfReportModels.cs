using iTextSharp.text.pdf;
using iTextSharp.text;

namespace DotNetCoreBaseWebApi.Models.PdfReport
{
    public class PdfReportModels
    {
        private int totalColumn = 3;
        private Document? document;
        private Font? font;
        private BaseFont? chineseBaseFont;

        private MemoryStream memoryStream = new MemoryStream();
        private List<StudentModels> students = new List<StudentModels>();

        public PdfReportModels()
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            string dir = Path.Combine(Path.GetDirectoryName(Environment.CurrentDirectory), "Font");

            // 註冊字型
            FontFactory.RegisterDirectory(dir, true);

            var builder = WebApplication.CreateBuilder();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                chineseBaseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}/Fonts/華康字體DFPOP2W9-B5.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }
            else if (app.Environment.IsStaging())
            {
                chineseBaseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}Fonts/華康字體DFPOP2W9-B5.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }
            else
            {
                chineseBaseFont = BaseFont.CreateFont($"{AppDomain.CurrentDomain.BaseDirectory}Fonts/華康字體DFPOP2W9-B5.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            }
        }

        /// <summary>
        /// 生成學生資料表
        /// </summary>
        /// <returns></returns>
        public List<StudentModels> GenerateStudentList()
        {
            List<StudentModels> students = new List<StudentModels>();

            for (int i = 1; i <= 6; i++)
            {
                Random random = new Random();
                int randomGender = random.Next(1, 3);
                string gender = randomGender == 1 ? "男" : "女";
                students.Add(new StudentModels()
                {
                    Id = i,
                    Name = $@"學生 {i} 號",
                    Gender = $@"{gender}"
                });
            }

            return students;
        }

        /// <summary>
        /// 準備生成報表
        /// </summary>
        /// <param name="_students"></param>
        /// <returns></returns>
        public byte[] PrepareReport(List<StudentModels> _students)
        {
            students = _students;

            document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            document.SetPageSize(PageSize.A4);
            document.SetMargins(20f, 20f, 20f, 20f);

            PdfWriter.GetInstance(document, memoryStream);
            
            document.Open();

            this.ReportHeader();
            this.ReportStudentTable();

            this.NewLine();

            this.ReportImage();

            document.Close();

            return memoryStream.ToArray();
        }

        /// <summary>
        /// 插入標題文字
        /// </summary>
        private void ReportHeader()
        {
            Font TitleFontStyle = FontFactory.GetFont("Tahoma", 12f, 1);
            Paragraph TitlePhrase = new Paragraph("My University Name", TitleFontStyle);
            TitlePhrase.Alignment = Element.ALIGN_CENTER;
            TitlePhrase.SpacingAfter = 6;
            document.Add(TitlePhrase);

            Font SubTitleFontStyle = FontFactory.GetFont("Tahoma", 10f, 1);
            Paragraph SubTitlePhrase = new Paragraph("Student List", SubTitleFontStyle);
            SubTitlePhrase.Alignment = Element.ALIGN_CENTER;
            SubTitlePhrase.SpacingAfter = 6;

            document.Add(SubTitlePhrase);
        }

        /// <summary>
        /// 插入學生資料表
        /// </summary>
        private void ReportStudentTable()
        {
            Font fontStyle = new Font(chineseBaseFont, 12f);

            #region table header
            PdfPTable studentTable = new PdfPTable(3);
            studentTable.WidthPercentage = 100;

            PdfPCell pdfpCellSerialNumber = new PdfPCell(new Phrase("學生編號", fontStyle));
            pdfpCellSerialNumber.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfpCellSerialNumber.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfpCellSerialNumber.BackgroundColor = BaseColor.LIGHT_GRAY;
            pdfpCellSerialNumber.ExtraParagraphSpace = 0;
            pdfpCellSerialNumber.Padding = 5;
            studentTable.AddCell(pdfpCellSerialNumber);

            PdfPCell pdfpCellName = new PdfPCell(new Phrase("姓名", fontStyle));
            pdfpCellName.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfpCellName.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfpCellName.BackgroundColor = BaseColor.LIGHT_GRAY;
            pdfpCellName.ExtraParagraphSpace = 0;
            pdfpCellName.Padding = 5;
            studentTable.AddCell(pdfpCellName);

            PdfPCell pdfpCellRoll = new PdfPCell(new Phrase("性別", fontStyle));
            pdfpCellRoll.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfpCellRoll.VerticalAlignment = Element.ALIGN_MIDDLE;
            pdfpCellRoll.BackgroundColor = BaseColor.LIGHT_GRAY;
            pdfpCellRoll.ExtraParagraphSpace = 0;
            pdfpCellRoll.Padding = 5;
            studentTable.AddCell(pdfpCellRoll);
            #endregion

            #region table body
            fontStyle = new Font(chineseBaseFont, 12f);

            foreach (StudentModels student in students)
            {
                PdfPCell pdfpCellStudentId = new PdfPCell(new Phrase(student.Id.ToString(), fontStyle));
                pdfpCellStudentId.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfpCellStudentId.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfpCellStudentId.BackgroundColor = BaseColor.WHITE;
                pdfpCellStudentId.ExtraParagraphSpace = 0;
                pdfpCellStudentId.Padding = 5;
                studentTable.AddCell(pdfpCellStudentId);

                PdfPCell pdfpCellStudentName = new PdfPCell(new Phrase(student.Name.ToString(), fontStyle));
                pdfpCellStudentName.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfpCellStudentName.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfpCellStudentName.BackgroundColor = BaseColor.WHITE;
                pdfpCellStudentName.ExtraParagraphSpace = 0;
                pdfpCellStudentName.Padding = 5;
                studentTable.AddCell(pdfpCellStudentName);

                PdfPCell pdfpCellStudentRoll = new PdfPCell(new Phrase(student.Gender.ToString(), fontStyle));
                pdfpCellStudentRoll.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfpCellStudentRoll.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfpCellStudentRoll.BackgroundColor = BaseColor.WHITE;
                pdfpCellStudentRoll.ExtraParagraphSpace = 0;
                pdfpCellStudentRoll.Padding = 5;
                studentTable.AddCell(pdfpCellStudentRoll);
            }
            #endregion

            studentTable.CompleteRow();
            document.Add(studentTable);
        }

        /// <summary>
        /// 插入圖片
        /// </summary>
        public void ReportImage()
        {
            string base64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGR4aGBcYGBgaGxoeHhgXGh0bFxodHSggHR0lHx0YITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICM1LS0wKy0tLy8vLS0tLS0tLS01LS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAK8BIAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAQIHAAj/xAA6EAABAwIEBAQDBwQCAgMAAAABAgMRACEEEjFBBSJRYQYTcYEykaEHFCOxweHwQlJi0XLxFYIzU6L/xAAaAQADAQEBAQAAAAAAAAAAAAABAgMEAAUG/8QALxEAAgICAgEDAwMCBwEAAAAAAQIAEQMhEjFBBBNRImFxMoGR0eEFFEKhsfDxI//aAAwDAQACEQMRAD8Ar2IUPkamAuINJnsQTRDGddhrtXzjIannqSTQlgOHKbq01NDLVnMmw2HajcBwta0hL68l5116TRvBuE51qQhIWAQCszAneZrIcyqCLupoOFidREpArXDtErAG5phxvh4ZdW3nnLF4MXExO1aYJlxBKoGkzTlvpsSS4/qoyfi+ADSRlUTO1A4JpKgrOqCNNqIwnHIWCpGamGI4mwpYK2tulS5ZAvEg/mX4KTyH8RPiuF5EhWcGdqWKavbenmdteY7DQUsdaUoEtpJir4mNU0k6b1Ni2MlhWrVxBHpU+FxwSjKpN69i+IoLaUpTlV+9UNitXDQ7uCtcDxWICvKZJE/FoK1xHBXcMry30QVXEEH8qsPC+LPtMENOQL2IB96WeRicQtxa8yi3Enpp/LUpyZEY8qC/7xmRSuruJcXgSlJyaml3DeGqElfWunYjww03hy66/wA+WRsJ2HeqMuTT+n9WuRSFP7xWxnH32ZI9w9uQUf2yQf3rGF4YVnlE+lSoJUB1mJnaNKc8G4qnD2sTTe66LQ2ZTG+99RTg+EkrKcpmhOLYHyzBInpVoc46CsqbjMaqfGUKLhWs37UMObIz/UKEoxUCxBcOBmhSqU4izhgzFMFMGyjUn3FNjua2DIAbMz871JMLxJIScwqDE491Az+WoIOhqRzAAnLNMXHFraDSiClND3SCKH5mjnrcQIxSwrN1oxrHkEKnemIwycoGXSon+D5xIsBSnIjfqmc/UdT2IxanDmsa2Z4stNjpWmHaCUkf1bVGvCuL5ggwN6kFU6rUBLD8wTE4JWIUQlMk9KHTw8tHKpMEbVbOG8HxCGy8iBSZwrU4VLuTVFzN0P0wkkLvuKX8DJtReGw+QDNpRrjaZB0rXhmH89wpUeQUTlJXfQnBzdQZD2ZVvhH1olCEzI+Vb4zBBpeUabVoyyZkA+1JyBFjqMLDUZPjcOMoUnQ2PY0Cli3qaenKkAKiFC9LXgEECd9e0R+tcCRqUP3j3B8GYSQVrlSjAGw70MlgtOKg2SqAa3w/CXlwoJUUknmANo60E+pWcpub7VkUEk21yZaqNVGWPWpXxKkWq54Lj2CwrGVgqUTciFHmteTYVRmsK6qcjbi4EmEk2617DYpxIWUolITKh2nUdwelTyYFygL4+BGGVlNwzF8X8xS1K/qMxuZ2PdNTt40KaJBggfyaTOsKV+MLg30tUTRlN7AmD86qFFUsQM1xpisazkSkCFRzEDfeoVvWmZ2FBeIUNJy+UTMDN671CELDV0qHqCKZFHEN/wAzuTEkQjDuwoxedqsGB4391QcyEqzbVjhXiTDNNhP3aXAIJgGTSt7hGIxKnnfLKEtjMrNygbwJ3i9QN5SVyLS/fzHUlRamzB8fxFLilLy5ZIgbaXoVbwJmKwpmRlNj3qLHYVTZT0NaVCilmcsbuP8AhfGWGErLiM5gwO8GPaakY4z5TDDYzEnne1GZRUVGD7x7VVMYrMQgaqIHtvTXDOLXnABU8qAhIEgyeaehy6UrenUiz53v7f8AsqmU1X8S18V4FjcQlBLQECYzgmImlDWDcbUlQBQ4klOUiZUbCPnTlHjN5hsNuNhSxbMZBIAGv0vSfjHFCMQHVQpYSlfIZSDqB8qx4BnFihXipdxjB2d+YsxPD3sOr8ZBSDOulLcU8CZTV44j4tZfZ8goJdVBVnAhH+6qquArK0pZBcmcx2mdE+1asGVit5hxI/gyGXGAaQ3AkOwRGtZxBKhJNGL4K4JCkKSoAmDawoDzE5CNTV1KttZPiRozAxViDoKhw5nXrUBXykxFbYFK1fAJPSrFaEGyQIwfKAR1orCuoCCT8W1b8A4Ml98tvuho5ZE7npTvj/hRplCAy8XXVGMoINtzasmTMisEJ3KKHILSujEajannBeJJBCFDkIv3oHEeGn2eZaeU6maGU0pf/wAaVFR2FKTjyKeJ18wqHTZEI49hEJeyNAzrl9p/Ko8BxJSWiARBMRvWMb5jSg44kpWEADvFqSqlSSZiDp1mq4l+kC/3iFjyLES0YXxBlhtR5TtUMt+YTsarRSTBFyKt2Ix+HOHSlCIci5rshKUALuOj8v1HqIOJKDioAgJrRlflplOtHNYR1yShpRH9RANO/DPhMYhOdTgCQYKYvNLkz48SW51AqMz/AEysKdLxzK2tTnDcUSw0pGQKJETV1w3gvCtt5CZUojmmDSLEcCQ/iltM2bQLqmaxr67BmtfA3NRxOtN5M5/jFOKUNagxGJkgG19a6Xxbwp5PlttnMXTlk7TvXsH4RZLWJUqFeWspB7IQnN/+s3yrYP8AEsHEN/3up3ttyA+JLjvHLbbZCG1yUxEAAWj3queFcQycQlT6lIT2TmknQHp8qRKezK3N4ozD4UhCXDImSkyIOUwRa4M1NPR48SEDz2ZnOd3I61Oi8T8QIaSfuy0LSAoFMHMAqdFdAozHrSjw5w1xxj8QoDMq35uaxH+PW/Sq0rGBYKiIMQTJv+/+qZt+OHG2UshtBygDOqTPSRpaon0+RE44tk9kx/eVmvJLJ90wWFbQy6orURITMlQKoAAHWrEODYcjL5KRawUmDAIsTG1cqcQcQo4h3EALiRFoKdAI0ps74sxSUNgP+aki5KRmBHxJO+kEE1nyejyNRVvq89gX9tSqZ1HY14/E99oOAwzCmmW7vKKlLM/0mcs9L6DoKdYDiOHxLCGsUAlSUCFDa0TVFxLBfxGZSz8KiVyDdIkBU6VI8pJACFzMZo9L1rf03LGiljY3ciuUgkgCjLd4c4hg2PMb8oPKMw4ACpaZNgk2FumsV7xd4xhnymm8oVHMq5gdB19dIqh/eikjJYg2O9TYh8OAlRlWpn6kU59KvuB2s/vE99uPETKlhabEyRRzOKYLRCwS5EJJpZgTCo60S1hkh4pdSqBII0M7VYgXUmveorxKD5qQNhb3p0VjCugznWAMwmwJGxG9L8PhirEZRc6CfpNWfhvhUOMqfddS2AoiTeYpsjrQDQYwWOohcx3nTmVJGWTvYQPW0esUPiVjNyzoP9ihVN+W4YnKd+o2qZsOIczJjS5IlNx+cU3ADqcSSdyQB1byleUtalIzKypJtIAiBvpTPgnHFtEXypSZSCND0roPA+NtqZaLZaaISElC7HXRJmSJk71SPHy0HF5UCJIOgEk7gV56ep/zGQ4XSv7TW6cFDq1wviXHl4gq8xAyJTqLTO1V/gnhnE4krLSAUJMElQAB1i+tNuJ8OUzhvxFpnMIA1NgR+dSeHcc6wsJYOZSrqRsoxAH1qqnhiK4K+3xJFuT/AP0le4vwDEsKy4hooQoci5BSTExI3qLw1kDozr8v/KrB4jx+PxryWnmFN5fgayqGtsxnX1qP/wAKvChX3pk5VpIChBhW19qv7jcPbykBj4H95xT6uSdCQ+IQ0nEAtr8yRzE/pU2AxC2Al0gpzg+WvUQLG2utLXcMA3GUZiRC5uEiZAHefpTPAYNTiMjbWdZKQlcnlF7Rpc0hVBjpjf5gJs2O/tJnvEL6w4hcFKmyLj05h3ofA+HscU+Y0haRBUFTl02Fb4nBqZcbbU3+Iv8AqOl7ZUDSNLmnnHMXi+HIabD2cLSZJEhJGqU9oqJPFQuHjv8Ag/xHUG+Tk6lUx2HfVl8wldpBmZzGZ9KnDeHLPlFB848sjSZ1mgmMSrMomYNhfQDasMpWp4BlCiqCqEiY6mtDKSNmq3rUkGHLXmWhHgPEJa5S3MSBJk9ppTgOBYhTvlhs50m/9ojqdKO4h4j4jhmkBYyykZVFIJg9xamXCPGJZZDeIbUHACpK0xzyZv0N6whvVqhalazqppYYWYDYmcV4qeQRhy0GosojWBrFDO4RKVheCxRCiJUCZBk9KU+IeOqddS8pCQnQJBvbr3vSfFvBuS2ogqNhuneDVsPpQQCRV9jsH8ybZqNd/f4jlzjeJLxzqJCLZtpHSvYXjLjSlFogFU5ldRrSfD4lZbKSZn50Jh1RN9BWj/LpRFCuqinKZZX/ABI6t4OLVPljliwBjalquOvqZVh80IcUVKO5zKKiCehJvQBehJAG1M+E8KSspStWUHf5V3t40Xaih1+0mXdmoHuW7ifhJhqPuuYupKRA5jrMknSBVVxz3MtMEKBktzbNoo+9EMeKHms65lawOYiR3kdIq18K8J4VaE4hx3Opd1KSoBMzJjpa1Yzlb04vMb+K3uX4jL+jU5qp4RAF5rysOpxJAGl6ceKEMIxJ+6wUJgxqCRrHUVYvDvB2nWziXAtpIscosDY5kmJTGs7VrPqFVA9Vf8yK4iW4ysY7ws80004og+YvIlKbmSkkGdIiPnSxGHKMQlCiUHMEuZhp1mr7464xLfkIM+XClZtVCLFJ3mZtVXXxNt5YU4kApSBluSR1Kjckd9qODK74+TCu4ciorUphPE+EobIUHQqJFugsCYpecqVZ42279RWzLZlagRAGh3k7VtisOoLSmBJAsdL9aC2NE3EYgnWpZfBfBMBiAS6rM6ZAQrlF4gpvzEXv30q04nhHDsK2tbyW8hWVgKAOUlITDY1vFgOprljhUytMJvuNbjcVHj3ZNzmGaUm51G3vNRf075GvmePxLLmCCiouFsYdsBbrRhBchCTOYJF7mT3FSK4ViYL5bWECCVkWIVuOtzULKsrYTfdXvpb2p3j/ABkt3DpwymgjkEqCtQBAERa8H2qrtk5fSL+fsJMBdk+JWmUqCitOomjcBxUNN5HZUFGYmyd9O9C4VxQC5PJlkDYKO477UIjC5kKWTJBG9u9UKq2m61ExWNiTccxZW6OhAmN53o/BIKRr8STKbXHalBwg+InkTqesbCmzb7mQqbIAUkBQ3AEkDS2xtTMtAAQg8jcy1ivJUFKSFGeX/G0CPSmWAw5XxRhbhC0laZBAIuDAI6TFJsViAlCFBvMVSJPNJIFwNhAt+dT8Ix+RxtZGikqPoCK5VCsHqENWvE694i8LtY9vKqG3UXQtIj2UBqKr/wBnXhd5jFuOYhEZBDehEk3UPbQ9zV8w4BIUNDce96ZBoEhUXAr0/ZDEMOxNbY15BpBj8GFpn+oCx39PQ1yTxm68tsNkLCQslRUAJg5QB2muz0Fj+FtuiFpmp+p/w/HlyDL/AKhGJPHiJ84Tl7mSI6V7hnG3MOqUKyz7g+oq7eN/Ba8Orz2uZEyR0v8Av9KoMJUtQiSDHvNYsmEC1cWJiPJTX8R1xHjz61JcUR5gBCV5RYf4g6etbca8SqxGFS04gFxJEOTeB270mddsNeUxFRKCSgnQ1IYEHHXXUUO1kX3IkFeVXrarP4d8SHCpBDSFkzm2VHr+lV3DuKCspMipW8SArT3o5sYyAqwsQo3E2DOm8C40cQAHQjKYATqbyQPkKrfi7hKUhOJS4MpJQluZIAkTJ7jSkP3R3kUJEzlgwTQmc5TnJ1MSd6x4vSe1k5IaHxLt6gsnFh+8jeZJg7UeMMHsOXUJOZv4k7kD+GhvvSrJgWv296m4Hjy3iInlXKT0voa9FL8zPqCsrFlJP9JtQpRrB117Uz4vhkpKyDeRyjcGlmCSSogdZ/1RHRMUmtTZ1RCYIvI/3UxcUoKSn4jEeo6Vs7GfqBM/KKJ4Xj22HQ7lCxopJ1jcp79DSk6sDc5BuLHnTyn2PrWUYy5AnL0m3yqLAuzIV/OlZzi/pBqnHxU7cJa4iUmQBKYMdb084l4ndcEEBKQmE5SYIO5HWqsFgpInQVPh5LYSqytup3ipvgQkMR1DyZQQJOMStSkpKswFhN4GtuleyJLiv7rj6bUOhYBkzAiesTf3ia6HxThXDGMOpaQFLUAUHOSqSLelSzZxiKgg71qNjx8wTfUp+FENIOpmT71qw6S8SoyAf0qLhmNKrH4r37jtUmFalTl7iFDvVOJ5ESe7k77hMZo+KUjehnCUu2g3tuJN9P5evPvpU6nKo80e1F8VZSHLGbXPU60o1oxj8yFGJ81whZyiDJi49vWldzJ3JoxDjaEkpBzxff8Al6k4dlKSrafyBP5036BYEDnUkJSBlCptKraGBalzGfMQnc67e/tU7GGWoLixKd6J4phfJZCJImCvqZG3QGiKBr5hT5jHAcI+8ICW1RG50MmMw6fvVz8I8HaS2W1hHmpKXUkKPmEJPQjQaSmQc1c74b4gcbbTlAQnNklPxxBMehBrTE8ScbxaVtuqkABKkmCAQLDtUs2B8ilAa+8umRU2Rc6txDhDGTz1wltKlYgzIkxlQkxcJAnlGtcoxz6VLcUnNkJ5cwAMa6DQdtqk43xN1xxKHVqUoJTmkmM0STGlMPDvCF4txDYIgXIPSbn0oemwui0xsmLmyDIaUTs/glSl4LDLVdXlJB9hFWUCgeF4JDDSW0CEgaTNGBwV76Chuah1U3r1YCqzVJ0ixDKVpKVCQRBFcW8T+B/uz5dC0hpauUE36+8D867dVR+03hynsGoIElKkq9gRUPUYwyGI6gicR4pg8izBCkm4IoNK5SEwLGZ6g1NiW1JVIUSFWvsdxQ4VJ5ddCP1FeV4mNquxGPC+Gl55ACSbTbtUTnDy08UPgoSbg62JtWOC8TeYXmkTlInt0rfiePS4AoDmgTeetSrJzI8RuSha8y247D4FtlLnnGUjlAVN4vA71QnHCvMCbC4Hreo8QBkKyb7Db/ug8KsqMnWm9P6copJYn8znyc/FQwEkGNxFDJcOWQb7e1FvHKYCpAvXn0ykPJ0JyqT0V/o1oWJXiG8ZXKEOex/nrWpUEJEfEsZvTp+teTh1FtTa+oUPQ0I5iBmBgkmInYTAtU6vUBkmJaWU8okx+9ew7YBGdQEagXNY4suCsAnSYFA4IlQJNgNT2FMqkpcNam2dJSVCxGv86VM1h1OAqbuLFVwIHvQGJCwEkaH61KyOWUwI1Hr+9VK6sQgDsyYZQCBBV12n9al8wkAqiYN+poTFNApTkUM3SYv2NeeWUoCVAzH613G4CJIp0FJO4Nx261ol2ZSd7ih2F3/lxUq24JAOlxRKgagqoXwt3KpQ9xUynSFBXb9YNLkOEKnrRQkC/ePepsv1XO3JuIt5FpUnTUf6qM4lUgn+4VoFzE6DSsZgfZQHvM/rXV8zu4z4akrUvLAWJItMiTIAqFC8qSkgg70Mp5SIWgkFKtehmYpxxbENPNB5NlkgLSOp0Md6Vl1G/wBMHwmIU2uZBT06mNKzxdYWQJyqIGultqXNZVLSCbJv2trNHcUalKFjTSfW4pKpxOu4DwRorfSlYlM8yeoF/wBI96IaQhKm1rkqsAjuNJ9otQybKAOs/F+lexqj5y+xhPsIqx2ZxOp7EDnUokklZ/OuvfZhwBtX4+ckiwAER1nrXJMWmyFf5Zj8v91137IFKWlxQUcoIlMWFusmfpT4Rbgx8I+qdOKLUI6FDQTRwrxFelU2SheLPH7XD1JS+hYzTBF5jWIq2cD4u3iWUPNKzIWJSf58qF8SeFcNjUBGIbCwkyk7g9jUvBeDtYRoMsjKgEkJ6Tc0IY1CqHx7WdCkgwSCJGorVx6KGZxcqiKB+IJwPHtZCptUZgSLbwdTSHDKGfmt0O9dG+1DChGJStJgKFwIkq662Gu1c2cZaLkZl5idtvWvL9sBmWYXWmIhmJwuUKKRMxJGkdaDedCUgAb0/D2VGUXH51XMYnMDGuw60mPZoycFxbkkCbVlgwDBvQbaYF9SaP4YyfMkgwL6fKtTABYxEMxbgAy9AAT3ijeCtiFN6nLN9JHSlJkyVJIF9RE0Zw14JKSq0zeokUIOu5sMWpSiR3H6VslsZ8xGhSB0ga1EwnI5BGqj8qPYgAybXMdSr/Q/OpkV1CDuAtsOPLQ4kDKCcxJgATuaMcdZCYQA4lJE6gT67is8ZYUWiGiFACUoSIKdjI3NJuBYVd80hPQ9R2pyAV5X14jH9M1S9kaPOFCbiDb0J39Kn4ezIKwqUkGw/mtJnc7kZR7DQUyYV92QAqCV3MbCrOn067MYrrU0xaQBF7XrDCjlhXTfb0pkllLiCpMEwYpdlKUCRcC9IrWK8yfiCoRlVBv0/ajMQiQCNQL+lRLw8gObfy9T4dQiJuKdj5hO5EFZanaXJmdRMdKhWJzWgDahsCuXZ2gihxsEzuMPSvY7Emtml2PdU/K1RYhzIpJgEi9SeZJFot+ZmkrVxZlp2VWAuMqx/SRpJA69RuJ1rGKZLJJBlJTy3v6KGxFjfYg71pgXIUT7H3P6VM6CpRQYKCmxFwlXePkZ2NU81KCupjDrCUkn+sfQ2ijcZjPwAnoR+RpY9h5BCjBsIm9qMS0FJIOkT8qiwFgxJpg35ItuJFexypcnZRNR4PCKMOpKShJlQBumx1FG8GwC8UUtNoJWVW/W/SmK0bENQnhXCncY4zh2gZUrmI0CQbma+jfDvBW8IwhhsWSNTqTuTQ3hTw41g2UoQJVHMsxJO9+lO5rbgxcBvuasWPiJ6axnryhQryoq0rJXcQBQhfmhnVVqpwCl5ToPxPEQkyJBF4MVV/BDygXEqcdcQlUNqcIJywNTJKoMie1OeJJ805BdO/f1qs+L8YrCBDKE5c6ScwiwECB6VDJkr6viULhENxT4+4l5mIIQoHKBaPyO/tVKx4/FQqAJN7bxUiMQXCc3xCx+dB8TfILY5rKk9h3rACWyH7zzS3JjDnVwm9V3GrhRTuLDqKs2DAUsE/CgSe8X+VVZ5zzcQpSU6q3p/TjZuBR5huEw4VC1j+dTTliVCdtB86xhsHygn4flP7V7FYlLImQlJ29f7aRmLGouzBeOYgpRBjmsBNJGnJkRppUPEMaFqkzE2B1AovBMhcQMtpv06noPzrUuPgm5ThqFspzLSFEggSf+IEmtlrMEi5Wox2AtWj2IhwobHMRClkXNtANhUbbxS8EQYQL7z+8mp8f6wcZurEKSQUkhSbT/AKpp5xdIKU5XN40UO/eonMOkGVn0SNfc7fnSl/iLpKeXkmwTYC+/70AvLQgVSxm+EwyAk/ib9O1D4xsFSVeci2ygRUqglIykybT770DjcP7p2I0NXx7a5Re7jnhjWTRSYNxBofiasp1EGg8C5DgMEpT0Bt60RxMJudladjU+FZN+YpU3MNv/AIeQREaDUV7DmTYCZkR+V6Vtm+tFlOQXHfWqlK18wlahWNVymKE4XykCa0bxB0OlYvMgfWuC0pWcAaqGYt+SklOxB+eorZPPBTsYoJClKITEkm3vRfnoQIEKhVzoCYj1j86BWqAhC/M1xL4SbbGaxhMaVrKSeU6jT5UqdXJohgad6p7YAjcahzwKFhStFp9pFpHqIPzqZzHgNlO5ETW3D0F9vy1RmEkSdCAT8je3WgWmVRCkwdUnrUyoJ34isATcZeBx5j4ZghCyEmNb2r6i8P8AA2cK0htpAASNYvfW+tfOX2b8PIxzaREqMhJI0G5B+g1r6fbNhV8YHImXQDubVGdZqQ1Co1Yyk2LlQuJmvLXFaNkm9CdBcQxQqsLR+IGlaKpSoM6QYZgCqj9rGBzYYPJVlU3oIkEEi0df91dUVzf7X+MhhKEKTmQsGRGsaQdUnvSZFBWqivsSjcN4Oh/K+3iEpi6218pB33070FxPhiM4LmLbMmyUBS56AkWquFRkqQZQrZREj1rVrErzbWFovWX2iDYMy8JbMS8ltrIlUrWIkaADWl6VtMpzKv8A2oGqv270FhsUgiVkjawmpcmHW6FLxEwBykEQBoJ2FSXHWjf9YAI34X5uICXFnKgnQCSRsBVnZ4Zh4BUw2barGYx76UqHEsqAMOgOf8FJt9ZpfxHiz2U5APMBuk7DtPxUlNetTq3PeI+CsFSVMthKpgoFwe4Hbppeov8AxrKEw6+lF5KUwpRP+RGp+gpMwXnUL51hSjE6XGqSYmI0AoPDcFcKimRIE+vpWitUzxtVRMsfB3GkvrciUgGCqL7C3saS8S42lbpUhIQDaUjub1HolXMDFozCT/sUHg2pdQjIkgqAOp3vTY8Y7M5BrcbM4RwN+YSpSTJA09zW/DcWDYtcxHLexPcV0VrBIaYUpQ0BmRNgP58q5y00lcrbg5rgCyr9Qbx6Uo+q7EbGnKSM8NKQlKgkqcgRPMknqNtqIxHh91oOBYGUC4T1gRbVJA/MVBjHEtNLU68TilEFKAmPLEG+fWb6UBiMfiUMNgJWlAk5iTzZiCdb3Ndxc9GN7dwhphYByEWEZYuRFyetK3lKIjMCJkaUa68Q6W0s+YFc6bqnKoA7HQTHtUry8Ogp/CSFJ6E5R6hWpp6K7O4tVAkYRLY8xYjcJNBPYrMonrTDG8RbVH4Sl/8AsoCfYXqc4ZtKcy20oOuVTivyH6VRb7YbhA8tEYXvU7ClJOaDG9qYKxzYEowwVtmAXl+etD/+XUopQGm0X2Bn3JOlNsjqHiT4kqCG2iu4KpCOyf6j6/0/OlWbMlR6RVnxKELUklBUkQANAkd/U/nWcUwDYJACde/aBU1etkbM0LhPmVRBGaplPQIj3po7wtCjplv/AD19qGe4SU3JI+pHqKrzUxXxEdwIYoggjWmOKdVyupJg/RW/z/3QwwxMFkhUbSM3yNb4V+6m3ZBV1ER3jrN/+6NA9SfEHqdK+wTCFzFuuqSCEIso/FJN4rvqK5R9g2HLeFdKxCi4R8unaunF8Uy6lFFCELX3rTNQSn9TUQxckRvRhjLIDWHTlECtG3RFjPpWUpm5/wCqM6L3sWQuI2rIekVrj25Ep+IXjr2obhy/M7dqXzDDEpneK+fftb406vEuYdwAeWYQYuUkDfpX0WpkZSD0r5c8dJcaxbja1Z8pIQZzEJmwCjeI22riNwVcqyW1dDU6ARf6b/SpuH4VbziW20Fxw6IgknfrXS+B/Z02Efj5nHDcoQsoQnsFRzdDHtQdgO4wQNOfYBlbpDbTSnFH+lIkyfTT1NWPhHgnHKPOyhJyynMUTM2BAO43NdkwPDPLSEoSltAgEISkD0gRPrR6cANTre897VAtegI6oFnzvxLwxjGlmcOtJF5A5Y65hamPDPDnE3ADmbyETLriCI9pIrvS8ICIPSLb15eDEi2ljoJt03Eiu5WKIEU4wTuciw3gh4g5sRh2j1S6oifRSYFZwfgF1cF7EqULwppKHZHssK+ldaVg0f8A1puL8ovJ0rZjDpTAAA6QkR8wKnXxF9lBOdYL7LMMs5xiHSdeZrKPkdqPwv2WYdEnzlKO3KBHyq/5AJ+dqwoiY6/Sm/MYY1lbc8Ll5HlPqV5cf0HKVf4k6pB3qv8AFvsjw5IWw861H9JOcD0JuK6GHB39dq8FmdiKINaEIQDqfJbeKUFZpkzPNe/UzVg4jiR5TKYBU40FlRJsrOsH1EJFqhY4ICSC4EiNcs36Eaj1qPHYRSg2iByIISqbLGYq9jfSqFkciJwDCxC2eLKaaAEFYlBUkAqyyVJ5jYASrY6UtxzjRAUHFrcUAVW3i4k70VwNhpSVSHVEa5UDIP8Amom3pTVsBxzKw2gkC+Qny5BEFwx6EBOu9An66oyqYwovUrpxy2xlSFJV1VqnsBt717gjbS3k+e4QkqSPWVAXUfhAuSav/CfCilLK8SorWq6jEC28Ac216dL4SpKXF+UgIbSCnlSCCAYKZMZgCr5ig2YIDQk3Fbgy+DYZoBDaEjZRcKiVEzZEK1iJVsL9qr/FuBqWtFmWsOVAEtqByZp+JSrzMCYi4ozHOIBXmhAARJUFkpAzAgFIgriLEgSDE3NQY5pxzy/JByZEmwnNc3WBa0aidfliXJlB2ZAZGvcmxDWFbQ422VqUCfLXMqMEC4Gg96UviCc2adRce2naj8Lw5IGcpKoSpZ8tAEAbytQ29ZqPH4nDhKczThUpFlKWhIIkgKUEpg6ER2g1THfzcpiyEmoCROuneOm31obGpSgSkpKza5JOm9HcJwasS+2ygpClnLJ0TAKlKMaAJBNQu40JbW2hLapJAdySpaZsRJ5RAB5YPWasPvNQlfXzXVr10I+Vb/eLZV8wGk6j0UL/ADqRUHv8qiUkHp/O1VBisqmdF+zfxUG5ZJnNdKiQNNvbT0iuk4PxQlRyQZ3i9fNMKSZT9KfeFONOB1A/GcIzShIKgZ3MX6U4BAkarU77ieJcpUQYGv71WeIeLUIUAVwTeN46+lVfH+MXktOJ+4PBZHOcqskgXk71WnPDuKxP47gyFVwNyOxnQaUtnsx6Hid/8LcbafRmSVHrP/dWVtYOlfP32fcOcw+MbU44Q2TBSSYJ0BIr6EQRFqorXEIqJPEhytLWDEJJ9IvIqu+CuMB9RIIkiT3J3qzeLsCHsI8jOG1FtWVZ0SYN1f49e1fPvgriOPwSlISzmTPMZuIMHKd9orm1uFd6n0tlBF964N48+zQjFqWMRKHSVJQlAK0knSCsAjuIParjh/GizCvJfJSCChKRAPUnc9u9EPqXiClbqcouQLEpgycxj6VJs/xHGP7yqeC/D7WDSooPmrWB+KpJAInRJTOUHXW510q7tPwJKR1N5tvqPy/WosUEpRb4rRYkb3sZNgbdqLbCNSk3vBGUj1k26VBiSbjChoSNHFM0hFwDClGcqexj4ldhpqYsCaMSZ0EdTI3/AOOgF6HbLaYQlIyzsRF7z161K2+kyB6SCROxiOmlAXOqTKQiSb9za1havI03P0/7qMNbgWJi6uhmwII1kdflUoWddyPkdNe2lPOmqcQiQkkSdiNoOnyJ7VMQNJ12P1qNphIUVxc2JnpsBp71tGsEgm5739K4QTKwNJMdf3/esrakCDbSItWGTe/XlIjfUG2v53rby99ek2n5afUUe50FKkzpG830H827VA7iUiSpJSgG6wJi+irWHe4ogg5VkG8aKNrSJFtdLHtWWEykApJAFiVEm4nb5TSEQziC/DuKQ2FPN5SBzZoRfoiTKvUCKiS4yhvK7m813eIIA6Eg8x/uiY0pg3wLEu82ZAUYs7KjrcykkegkaVa8H4ZeSiA+VOJEgG6Cr0VJA21mplT3JezRsGpUBwtxI8xxRS18IQlMJBVygqn4lX1PerZwrh6UoCEDJAG1h7xfvW2Gxi0rCHwlKyeUkBaFeoBBHrTbG41aArOnLlAzKbhWUGbhKst/nXB/mUBoVdwZxShCAcy4sDAAGxUYsNe52FI/E3mAthbme5VpEqtJy7Np1AO8UUnxIlMoZYUqDOdSwCo/3KMySaUcQP3h0eapLa1AASguI1tvIN+hFK7giSy2woRHh+IqdeOFiEKCszh5inMDzEgwZMC+ntUXFcTiEOuhlXlIQmyFxBAnmFoBNzIpqx4YfYWShaJVEOCZEGQQCOsTbQVl/wANlxxSMQ8p5afiCAGmx6mCo66BNAFOXipAAn7Su4HGYt4p8nzE3nlklaonm6ARaRFN/EoddaS4+2EPpUqUx/StRiPkDf8AuNWVDKWQlltaUN5edLKVlaxG7iymLeuntSHGYgpQTgWOVYMvOKSpZygmIVGUiDoNd6uOJNqBKLyBqAcLBaw+KWAUvKbDLWY5TDivxFCeiBH/ALUkeQoWVl00Bn6iwonxLhlIZwZMl1xsuuKJmc6jkHskT/7Gl3DMI4+vy0AqWdpA0jckCrKprcsGNwbKbDt7fOp8DgVuryoQpV45RMnoNv5er/w37OwtsB1zK4YPKkKCQSIAnfWrPwHwdh8KUfEtYKilSiYTOpATFyAATG1dyEY3KlwP7PlZx54ISepBPUiEiBabknS29dA4dwBLZCy4pSzAlREDqAkADSYkU0wSgsyAUkDfQ7CwOw69ay+LiTuOUCZt3gARI99akzQ0JCpiTEkp0BN5M6Rv71G+wk2tqdQYjeEjXXTvR7BlUCxtrc/r1it1Mgqy5QIA/wBfLtQG41xSrhgIClJsLq1nTQTt77RW7mOxTfwJS62JjmIIgCCTER0vTVRSbnST110ryQQobiwI9ukd6NQXEnEcfiXkKSrDmFoKVAu3gi4QBYjW9taVYbhrqUwhltsDUuKzExF8qZ/MVdFNgSAI3N5sdfrQ62YRzXj6jSDe89z0rqg7ifB4JwJSVK59JSAnXXKm9rgd4FTIbKElNx8rdpF9b36mizlSmRJuMoJNom4MmK2ViAFEED1uRPTr70I0CewhWRC4CUyRHbY6i35UelJMc3bTatUlpV4Iix1mtXlG5bM3i+vtNdABCm8EmxV1vFu0/wA60QhsHTbQUnGMWgKUuAE21zXgHSB661LhOJNrQXEkmBmVbKe/bbrRG4DGym4uKidTbX1+Y1g3NeSVmAbgx8tzMk6Vqq0A2ue+g0FE3AJh6RHKDf21F++sV4OyQI9DvbfTpvWyG5kyLnv9P36VtkGoGlgbfX6URDMKUZkj3vHt3rKn0oSCTAEXOvQTAitkgidZvPb0rVlISQDJOk+vvR6gm+s2BFj2PatGmlAGwA2jbv3FbJHxTIgxe4+VRF7KAVRGg1uJjbS/5106f//Z";
            byte[] imageBytes = Convert.FromBase64String(base64.Replace("data:image/jpeg;base64,", ""));
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageBytes);
            image.ScalePercent(185f);

            PdfPTable imageTable = new PdfPTable(1);
            imageTable.WidthPercentage = 100;
            PdfPCell imageCell = new PdfPCell(image);
            imageCell.Padding = 5;
            imageCell.HorizontalAlignment = Element.ALIGN_CENTER;
            imageCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            imageTable.AddCell(imageCell);
            document.Add(imageTable);
        }

        /// <summary>
        /// 換行
        /// </summary>
        private void NewLine()
        {
            Paragraph paragraph = new Paragraph(Chunk.NEWLINE);
            document.Add(paragraph);
        }
    }

    /// <summary>
    /// 學生模型
    /// </summary>
    public class StudentModels
    {
        /// <summary>
        /// 學生 id
        /// </summary>
        public int Id { get; set; } = 0;

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public string Gender { get; set; } = "";
    }
}
