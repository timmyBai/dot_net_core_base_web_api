using System.ComponentModel.DataAnnotations;

namespace DotNetCoreBaseWebApi.Models.ValidateModels
{
    public class ValidateModels
    {
    }

    public class RequestValidateStateModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string password { get; set; } = "";
    }
}
