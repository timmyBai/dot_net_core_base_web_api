namespace DotNetCoreBaseWebApi.Models
{
    /// <summary>
    /// 版本控制模型
    /// </summary>
    public class PackageJsonModels
    {
        /// <summary>
        /// 專案名字
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 版本
        /// </summary>
        public string version { get; set; } = "";

        /// <summary>
        /// 描述
        /// </summary>
        public string description { get; set; } = "";

        /// <summary>
        /// 維護者
        /// </summary>
        public string author { get; set; } = "";

        /// <summary>
        /// 許可證
        /// </summary>
        public string license { get; set; } = "";
    }
}
