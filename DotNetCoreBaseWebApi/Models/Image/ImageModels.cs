using System.ComponentModel.DataAnnotations;

namespace DotNetCoreBaseWebApi.Models.Image
{
    public class ImageModels
    {
    }

    /// <summary>
    /// 圖片大小 接收模型
    /// </summary>
    public class RequestCalcImage
    {
        /// <summary>
        /// 圖片
        /// </summary>
        [Required]
        public string picture { get; set; } = "";
    }
}
