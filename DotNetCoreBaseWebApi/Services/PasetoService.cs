using Org.BouncyCastle.Asn1.Ocsp;
using Paseto;
using Paseto.Builder;
using Paseto.Cryptography.Key;
using System.Security.Claims;
using System.Text;

namespace DotNetCoreBaseWebApi.Services
{
    /// <summary>
    /// PaseToken 服務
    /// </summary>
    public class PasetoService
    {
        /// <summary>
        /// Paseto Token 本地私鑰與公鑰
        /// </summary>
        public PasetoSymmetricKey? SymmetricKey = null;

        /// <summary>
        /// Paseto Token 公開私鑰與公鑰
        /// </summary>
        public PasetoAsymmetricKeyPair? AsymmetricKeyPair = null;

        /// <summary>
        /// Paseto Token 私鑰種子
        /// </summary>
        private string PasetoSeed = "eijei&Yd*9h])5Vjh=RjB62nmiCWKf{H";

        /// <summary>
        /// 時效分鐘
        /// </summary>
        public static int Minutes = 30;

        /// <summary>
        /// 發送者網域
        /// </summary>
        public const string Domain = "http://localhost:5000/";

        /// <summary>
        /// Paseto Token 結尾
        /// </summary>
        public string Footer = AppDomain.CurrentDomain.FriendlyName;

        /// <summary>
        /// 亂數預備字串
        /// </summary>
        private const string RandomCode = "ABCDEFGHIJKLMNOPQRSTUVWXZYabcdefghijklmnopqustuvwxyz0123456789~`!@#$%^&*()_-+={[}]|<,>.?/:;'";

        /// <summary>
        /// 取得開發環境之環境變數
        /// </summary>
        private string? AspNetCoreEnvironment = "";

        /// <summary>
        /// Paseto 使用版本
        /// </summary>
        private ProtocolVersion protocolVersion = ProtocolVersion.V4;

        /// <summary>
        /// Paseto 使用版本與區域類型
        /// </summary>
        public string PasetoType = "";

        /// <summary>
        /// 初始化
        /// </summary>
        public PasetoService()
        {   
            AspNetCoreEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (AspNetCoreEnvironment == "Development" || AspNetCoreEnvironment == "Staging")
            {
                PasetoType = $"{protocolVersion}.{nameof(Purpose.Local)}.".ToLowerInvariant();
                GenerateSymmetricKey();
            }
            else
            {
                PasetoType = $"{protocolVersion}.{nameof(Purpose.Public)}.".ToLowerInvariant();
                GenerateGenerateAsymmetricKeyPair();
            }
        }

        /// <summary>
        /// 生成 Paseto 本地私鑰與公鑰
        /// </summary>
        private void GenerateSymmetricKey()
        {
            SymmetricKey = new PasetoBuilder().Use(protocolVersion, Purpose.Local).GenerateSymmetricKey();
        }

        /// <summary>
        /// Paseto Token 公開私鑰與公鑰
        /// </summary>
        private void GenerateGenerateAsymmetricKeyPair()
        {
            AsymmetricKeyPair = new PasetoBuilder().Use(protocolVersion, Purpose.Public).GenerateAsymmetricKeyPair(Encoding.UTF8.GetBytes(PasetoSeed));
        }

        /// <summary>
        /// Paseto 加密資訊
        /// </summary>
        /// <param name="claims">加密資料</param>
        /// <param name="audience">接收 token 者</param>
        /// <param name="subject">用戶 id</param>
        /// <returns></returns>
        public string Encode(IEnumerable<Claim> claims, string audience = "", string subject = "")
        {
            if (AspNetCoreEnvironment == "Development" || AspNetCoreEnvironment == "Staging")
            {
                return GeneratePasetoLocalToken(claims, audience, subject);
            }
            else
            {
                return GeneratePasetoPublicToken(claims, audience, subject);
            }
        }

        /// <summary>
        /// 生成本地 Paseto Token 
        /// </summary>
        /// <param name="claims">加密資料</param>
        /// <param name="audience">接收 token 者</param>
        /// <param name="subject">用戶 id</param>
        /// <returns></returns>
        private string GeneratePasetoLocalToken(IEnumerable<Claim> claims, string audience = "", string subject = "")
        {
            var pasetoBuilder = new PasetoBuilder().Use(protocolVersion, Purpose.Local)
                .WithKey(SymmetricKey)
                .IssuedAt(DateTime.UtcNow)
                .Issuer(Domain)
                .AddFooter(Footer)
                .NotBefore(DateTime.UtcNow)
                .Expiration(DateTime.UtcNow.AddMinutes(Minutes));

            foreach (Claim claim in claims)
            {
                pasetoBuilder.AddClaim(claim.Type, claim.Value);
            }

            if (!string.IsNullOrWhiteSpace(audience))
            {
                pasetoBuilder.Audience(audience);
            }

            if (!string.IsNullOrWhiteSpace(subject))
            {
                pasetoBuilder.Subject(subject);
            }

            var token = pasetoBuilder.Encode();

            return token;
        }

        /// <summary>
        /// 生成公開 Paseto Token 
        /// </summary>
        /// <param name="claims">加密資料</param>
        /// <param name="audience">接收 token 者</param>
        /// <param name="subject">用戶 id</param>
        /// <returns></returns>
        private string GeneratePasetoPublicToken(IEnumerable<Claim> claims, string audience = "", string subject = "")
        {
            var pasetoBuilder = new PasetoBuilder().Use(protocolVersion, Purpose.Public)
                .WithKey(AsymmetricKeyPair.SecretKey)
                .IssuedAt(DateTime.UtcNow)
                .Issuer(Domain)
                .AddFooter(Footer)
                .NotBefore(DateTime.UtcNow)
                .Expiration(DateTime.UtcNow.AddMinutes(Minutes));

            foreach (Claim claim in claims)
            {
                pasetoBuilder.AddClaim(claim.Type, claim.Value);
            }

            if (!string.IsNullOrWhiteSpace(audience))
            {
                pasetoBuilder.Audience(audience);
            }

            if (!string.IsNullOrWhiteSpace(subject))
            {
                pasetoBuilder.Subject(subject);
            }

            var token = pasetoBuilder.Encode();

            return token;
        }

        /// <summary>
        /// 生成 refresh token
        /// </summary>
        /// <returns></returns>
        public string GenerateRefreshToken()
        {
            string refreshToken = "";

            for (int i = 0; i < 256; i++)
            {
                Random random = new Random();
                int index = random.Next(0, RandomCode.Length);
                refreshToken += RandomCode[index];
            }

            return refreshToken;
        }

        /// <summary>
        /// 解密 Paseto Token
        /// </summary>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        public PasetoPayload? Decode(string token)
        {
            PasetoTokenValidationResult? pasetoTokenValidationResult = null;

            switch(AspNetCoreEnvironment)
            {
                case "Development":
                case "Staging":
                    pasetoTokenValidationResult = DecodePasetoLocalToken(token);
                    break;
                default:
                    pasetoTokenValidationResult = DecodePasetoPublicToken(token);
                    break;
            }

            if (pasetoTokenValidationResult.IsValid)
            {
                return pasetoTokenValidationResult.Paseto.Payload;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 解密本地 Paseto Token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private PasetoTokenValidationResult DecodePasetoLocalToken(string token)
        {
            var pasetoTokenValidationResult = new PasetoBuilder().Use(ProtocolVersion.V4, Purpose.Local)
                .WithKey(SymmetricKey)
                .Decode(token, new PasetoTokenValidationParameters()
                {
                    ValidateLifetime = true,
                    ValidateIssuer = true,
                    ValidIssuer = Domain
                });

            return pasetoTokenValidationResult;
        }

        /// <summary>
        /// 解密公開 Paseto Token
        /// </summary>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        private PasetoTokenValidationResult DecodePasetoPublicToken(string token)
        {
            var pasetoTokenValidationResult = new PasetoBuilder().Use(ProtocolVersion.V4, Purpose.Public)
                .WithKey(AsymmetricKeyPair.PublicKey)
                .Decode(token, new PasetoTokenValidationParameters()
                {
                    ValidateLifetime = true,
                    ValidateIssuer = true,
                    ValidIssuer = Domain
                });

            return pasetoTokenValidationResult;
        }
    }
}
