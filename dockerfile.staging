FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build-env

WORKDIR /app

COPY /DotNetCoreBaseWebApi/*.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:8.0

WORKDIR /DotNetCoreBaseWebApi

RUN apt-get update -y \ 
    && apt-get install -y libgdiplus \
    && apt-get clean \
    && ln -s /usr/lib/libgdiplus.so /usr/lib/gdiplus.dll

COPY --from=build-env /app/out .
COPY --from=build-env /app/DotNetCoreBaseWebApi/Fonts ./Fonts
COPY --from=build-env /app/DotNetCoreBaseWebApi/Assets ./Assets
COPY --from=build-env /app/DotNetCoreBaseWebApi/FRXReport ./FRXReport
COPY --from=build-env /app/package.json ./

ENTRYPOINT ["dotnet", "DotNetCoreBaseWebApi.dll", "--urls", "http://0.0.0.0:5100"]