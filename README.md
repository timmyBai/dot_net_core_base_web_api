# DotNetCoreBaseWebApi

<div>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0">
        <img src="https://img.shields.io/badge/C%23-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0">
        <img src="https://img.shields.io/badge/.NetCore-8.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet/8.0">
        <img src="https://img.shields.io/badge/WebApi-8.0.0-blue">
    </a>
</div>

## 簡介

DotNetCoreBaseWebApi 是一個建構基本 Dot Net Core Web Api 架構後端解決方案，他基於 C#、.Net Core、Web Api 實現，他使用最新後端技術，內置了 token 登入權限解決方案，跨域連線，Swagger，輕量級、高效能且模組化的 HTTP 要求管線，跨平台相容性，提供了許多後端處理方法，他可以幫助你快速搭建後端原型，可以幫助你解決許多需求。

## 功能

```tex
- 會員登入與註冊
    - 會員登入
    - 會員簡介
    - 會員令牌重製
    - 登出
- Excel 匯出
    - 匯出 Excel 檔
- 圖片
    - 取得圖片容量大小
- PDF 匯出
    - 匯出 pdf 檔
- QRCode
    - 產生 QRCode
- 接收模型驗證
    - 資料收模型驗證
- PDF 匯出
    - 使用 itextSharp 匯出 pdf 檔
    - 使用 fast report 匯出 pdf 檔
- 圖片
    - 取得圖片容量大小
- WebSocket
    - WebSocket 處理
```

## 開發

```bash
# 克隆項目
git clone https://gitlab.com/timmyBai/DotNetCoreBaseWebApi.git
```

## 本地發佈

```docker
# development(開發機)
docker-compose -f docker-compose.development.yml build && docker-compose docker-compose.development.yml up -d

# staging(demo 機)
docker-compose -f docker-compose.staging.yml build && docker-compose -f docker-compose.staging.yml up -d

# production(正式機)
docker-compose -f docker-compose.production.yml build && docker-compose -f docker-compose.production.yml up -d
```

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
