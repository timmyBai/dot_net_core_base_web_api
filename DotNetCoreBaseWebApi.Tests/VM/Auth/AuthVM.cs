using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreBaseWebApi.Tests.VM.Auth
{
    public class AuthVM
    {
    }

    public class AuthTokenVM
    {
        /// <summary>
        /// 核發 token
        /// </summary>
        public string accessToken { get; set; } = "";

        /// <summary>
        /// 過期時間
        /// </summary>
        public long expiresIn { get; set; } = 0;

        /// <summary>
        /// token 類型
        /// </summary>
        public string tokenType { get; set; } = "";
    }
}
