using System.Web.Http;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

using DotNetCoreBaseWebApi.Tests.Models;
using DotNetCoreBaseWebApi.Tests.Models.Auth;
using DotNetCoreBaseWebApi.Tests.VM.Auth;
using DotNetCoreBaseWebApi.Tests.Utils;

namespace DotNetCoreBaseWebApi.Tests.Controllers.Auth
{
    public class AuthController : ApiController
    {
        /// <summary>
        /// 會員登入
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public ResultModels AuthLogin(RequestAuthLoginModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                return result;
            }

            try
            {
                string userName = "timmy";
                string account = "timmy";
                string password = "123456";
                string role = "student";

                if (body.account != account && body.password != password)
                {
                    result.isSuccess = false;
                    result.message = "AccountExist";
                    return result;
                }

                Claim[] claim = new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                    new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds()}"),
                    new Claim("account", body.account),
                    new Claim("userName", userName),
                    new Claim("_role", role)
                };

                AuthTokenVM authtoken = new AuthTokenVM();
                authtoken.accessToken = new JwtUtils().GenerateJwtToken(claim);
                authtoken.expiresIn = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));
                authtoken.tokenType = "Bearer ";

                result.isSuccess = true;
                result.data = authtoken;
                return result;
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return result;
            }
        }
    }
}
