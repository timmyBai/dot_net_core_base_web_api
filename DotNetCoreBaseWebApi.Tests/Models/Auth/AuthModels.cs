using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreBaseWebApi.Tests.Models.Auth
{
    public class AuthModels
    {
    }

    /// <summary>
    /// 接收 登入模型
    /// </summary>
    public class RequestAuthLoginModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        public string password { get; set; } = "";
    }
}
