using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreBaseWebApi.Tests.Models
{
    public class ResultModels
    {
        public bool isSuccess { get; set; }
        public string message { get; set; }

        public object data { get; set; }

        public ResultModels()
        {
            isSuccess = false;
            message = string.Empty;
            data = new object();
        }
    }
}
