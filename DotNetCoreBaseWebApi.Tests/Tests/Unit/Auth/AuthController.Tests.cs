using DotNetCoreBaseWebApi.Tests.Controllers.Auth;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = NUnit.Framework.Assert;

using DotNetCoreBaseWebApi.Tests.Models.Auth;

namespace DotNetCoreBaseWebApi.Tests.Tests.Unit.Auth
{
    [TestClass]
    public class AuthControllerTest
    {
        /// <summary>
        /// 測試驗證登入驗證登入無此帳號
        /// </summary>
        [Test]
        [TestMethod]
        public void TestAuthLoginAuthAccountExist()
        {
            RequestAuthLoginModels requestAuthLogin = new RequestAuthLoginModels();
            requestAuthLogin.account = "abc";
            requestAuthLogin.password = "123";

            var controller = new AuthController().AuthLogin(requestAuthLogin);

            Assert.IsTrue(controller.isSuccess == false);
            Assert.IsTrue(controller.message == "AccountExist");
        }

        /// <summary>
        /// 測試驗證登入成功
        /// </summary>
        [Test]
        [TestMethod]
        public void TestAuthLoginSuccess()
        {
            RequestAuthLoginModels requestAuthLogin = new RequestAuthLoginModels();
            requestAuthLogin.account = "timmy";
            requestAuthLogin.password = "123456";

            var controller = new AuthController().AuthLogin(requestAuthLogin);

            Assert.IsTrue(controller.isSuccess == true);
            Assert.IsTrue(controller.message == "");
            Assert.IsTrue(controller.data != null);
        }
    }
}
